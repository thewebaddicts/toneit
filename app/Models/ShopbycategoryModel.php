<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopbycategoryModel extends Model
{
    protected  $table='ecom_categories';
    use HasFactory;
}
