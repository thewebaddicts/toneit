var version = document.currentScript.getAttribute("attr-cache-version");

function loadCss(url) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
}

loadCss("/css/style.css?v=" + version);
loadCss("https://use.fontawesome.com/releases/v5.7.1/css/all.css");
loadCss("/js/owl.carousel/dist/assets/owl.carousel.min.css");
loadCss("/js/jquery.fancybox/dist/jquery.fancybox.min.css");
loadCss("/js/jquery.aos/dist/aos.css");

requirejs.config({
    waitSeconds: 200,
    paths: {
        functions: "/js/functions.js?v=" + version,
        jquery: "/js/jquery/dist/jquery.min.js?v=" + version,
        owl: "/js/owl.carousel/dist/owl.carousel.min.js?v=" + version,
        fancybox: "/js/jquery.fancybox/dist/jquery.fancybox.min.js?v=" + version,
        aos: "/js/jquery.aos/dist/aos.js?v=" + version,
    },
    shim: {
        functions: {
            deps: ["jquery", "fancybox"]
        },
        owl: {
            deps: ["jquery"]
        },
        aos: {
            deps: ["jquery"]
        },
        fancybox: {
            deps: ["jquery"]
        }
    }
});

//Define dependencies and pass a callback when dependencies have been loaded
require(["jquery"], function ($) {
    jQuery.event.special.touchstart = {
        setup: function (_, ns, handle) {
            this.addEventListener("touchstart", handle, {passive: true});
        }
    };
    var is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if (!is_safari) {
        $(window).on("beforeunload", function () {
            // ShowLoader();
        });
    }


});

require(["functions"], function () {
    try {
        wtspIcon();
    }
    catch (e) {
    }
    try{
        eliminate();
    }  catch (e) {
    }


try {
    FooterFunctions();
} catch (e) {
}
try {
    search();
} catch (e) {

}
try {
    closeSearch();
} catch (e) {

}
try {
    checkPass();
} catch (e) {

}
try {
    NotificationFunction();
} catch (e) {

}
try {
    removeDisplay();
} catch (e) {
}
try {
    doAccordion();
} catch (e) {

}

specialButton();
window.addEventListener("scroll", function () {

    try {
        MenuScroll();

    } catch (e) {

    }
});


window.addEventListener("scroll", function () {
    try {
        InitializeMenuScroll();
    } catch (e) {

    }
});
try {
    hover();
} catch (e) {

}


// doItem();
// try {
//     doSubmenu();
// } catch (e) {
//
// }
try {
    filterCustom();
} catch (e) {

}
try {
    accordion();
} catch (e) {

}
try {
    clickChange();
} catch (e) {

}
try {
    burger();
} catch (e) {

}
try {
    addAddress();
} catch (e) {

}


try {
    removeFeature();
} catch (e) {

}
try {
    editFeature();
} catch (e) {

}
try {
    doOrders();
} catch (e) {

}
try {
    doSub();
} catch (e) {

}


try {
    wisthlistHeart();
} catch (e) {

}


try {
    iconFormSubmit();
} catch (e) {
}
try {
    search();
} catch (e) {
}
try {
    filter();
} catch (e) {

}
try {
    filterBrands();
} catch (e) {

}
try {
    wisthlistHeart();
} catch (e) {

}

try {
    disableButton();
} catch (e) {

}
try {
    removeQueryNotificationString();
} catch (e) {

}


})
;

require(["aos"], function (AOS) {
    AOS.init({easing: "ease-in-out-sine", duration: 600, once: true});
});


require(["owl"], function (owlCarousel) {

    $(".carousel").each(function (index, elem) {
        var owl = $(elem);
        owl.owlCarousel({
            items: parseInt($(elem).attr("data-carousel-items")),
            nav: $(elem).attr("data-carousel-nav") === "true" ? true : false,
            dots: $(elem).attr("data-carousel-dots") === "true" ? true : false,
            autoplay: $(elem).attr("data-carousel-autoplay") === "true" ? true : false,
            slideTransition: 'linear',
            autoplayHoverPause: false,
            autoWidth: $(elem).attr("data-carousel-autowidth") === "true" ? true : false,
            rtl: false,
            autoHeight: true,
            margin: 20,
            autoplayTimeout: 4000,

            // animateOut: $(elem).attr("data-carousel-animate"),
            loop: $(elem).attr("data-carousel-loop") === "true" ? true : false,
            navText: [
                "<img src='/assets/svgs/arrowleft.svg'>",
                "<img src='/assets/svgs/arrowright.svg'>"
            ],

        });


        // $('.owl-dot').click(function () {

        //     owl.trigger('to.owl.carousel', [$(this).index(), 300]);
        // });


        // owl.on("changed.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
        // owl.on("initialized.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
        // bLazy.revalidate();
    });

    if ($(".single-carousel").length > 0) {
        var owl = $(".single-carousel");
        owl.owlCarousel({
            items: 1,
            nav: false,
            dots: true,
            dotsContainer: ".owl-dots-custom",
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: false,
            rtl: false,
            loop: true
        });
        // owl.on("changed.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
        // owl.on("initialized.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
    }

    if ($(".multi-carousel").length > 0) {
        var owl2 = $(".multi-carousel");
        owl2.owlCarousel({
            item: 4,
            autoWidth: false,
            autoplay: true,
            rtl: true,
            autoplayTimeout: 3000,
            loop: true,
            nav: false,
            // items: 7,
            margin: 20,
            // padding: 100,
            dots: false
        });
        // owl2.on("changed.owl.carousel", function (event) {
        //     bLazy.revalidate();
        // });
        // owl2.on("initialized.owl.carousel", function (event) {
        //     bLazy.revalidate();
        // });
    }

    $(".product-carousel").each(function (index, elem) {
        var owl = $(elem);
        owl.owlCarousel({
            items: parseInt($(elem).attr("data-carousel-items")),
            nav: $(elem).attr("data-carousel-nav") === "true" ? true : false,
            dots: $(elem).attr("data-carousel-dots") === "true" ? true : false,
            autoplay:
                $(elem).attr("data-carousel-autoplay") === "true"
                    ? true
                    : false,
            transitionStyle : "fade",
            autoplayHoverPause: false,
            smartSpeed: 1500,
            margin: 10,
            autoWidth:
                $(elem).attr("data-carousel-autowidth") === "true"
                    ? true
                    : false,
            rtl: $(elem).attr("data-carousel-rtl") === "true" ? true : false,
            autoHeight: true,
            // animateOut: $(elem).attr("data-carousel-animate"),
            loop: $(elem).attr("data-carousel-loop") === "true" ? true : false,
            navText: [
                "<img src='/assets/svgs/circle-left.svg'>",
                "<img src='/assets/svgs/circle-right.svg'>"
            ],
            responsive: {
                0: {
                    items: 1,
                    autoWidth:false,
                },
                640:{
                    items: 1,
                    autoWidth:false,
                },
                1000: {
                    items:  parseInt($(elem).attr("data-carousel-items")),

                },
            },
        });
        $(".owl-dot").click(function () {
            owl.trigger("to.owl.carousel", [$(this).index(), 300]);
        });
        // owl.on("changed.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
        // owl.on("initialized.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
        // bLazy.revalidate();
    });

});


require(["fancybox"], function () {
    $.fancyConfirm = function (opts) {
        opts = $.extend(
            true,
            {
                title: "Are you sure?",
                message: "",
                okButton: "OK",
                noButton: "Cancel",
                callback: $.noop
            },
            opts || {}
        );

        $.fancybox.open({
            type: "html",
            autoDimensions: true,
            src:
                '<div class="fc-content">' +
                "<h3>" +
                opts.title +
                "</h3>" +
                "<p>" +
                opts.message +
                "</p>" +
                '<div class="text-right confirmation">' +
                '<a data-value="0" data-fancybox-close>' +
                opts.noButton +
                "</a>" +
                '<button data-value="1" data-fancybox-close class="rounded">' +
                opts.okButton +
                "</button>" +
                "</div>" +
                "</div>",
            opts: {
                animationDuration: 350,
                animationEffect: "material",
                modal: true,
                baseTpl:
                    '<div class="fancybox-container fc-container" role="dialog" tabindex="-1">' +
                    '<div class="fancybox-bg"></div>' +
                    '<div class="fancybox-inner">' +
                    '<div class="fancybox-stage"></div>' +
                    "</div>" +
                    "</div>",
                afterClose: function (instance, current, e) {
                    var button = e ? e.target || e.currentTarget : null;
                    var value = button ? $(button).data("value") : 0;

                    opts.callback(value);
                }
            }
        });
    };

    try {
        NotificationFunction();
    } catch (e) {
    }
});
