<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'google' => [
        'client_id' => '283859042794-o7mubrs30lsg0lgmi3rbejt5ouhof260.apps.googleusercontent.com',
        'client_secret' => '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',
        'redirect' => env('APP_URL').'/ecom/users/login/google/callback',
    ],

    'facebook' => [
        'client_id' => '644605310708428',
        'client_secret' => '0573d206551aa8837e560e1b0907a74a',
//        'redirect' => 'https://qareb.com/login/facebook/callback',
        'redirect' => env('APP_URL').'/ecom/users/login/facebook/callback',
    ],

];
