<main id="second">

    <header>
        <h3>Addresses</h3>
        <button><a href={{route('account-add-address')}}>Add Adresses</a></button>
    </header>

    @if(isset($addresses) && sizeof($addresses)>0)

        <div>
            <div class="box">
                @foreach($addresses as $key => $address)
                    <form style="display:block;" method="get" action="{{route('delete-address',['id'=>$address->id])}}" class="icon-form">
                   
                        <div id="remove-address-{{$loop->index}}" class="sub-box">
                            <header><span>Address {{$key + 1}}</span>
                                <div><span class="edit-feature" data-target="edit-address-first">
{{--                                    <img--}}
                                        {{--                                            src="/assets/svgs/Icon feather-edit.svg" alt="">--}}

                                </span>


                                    <span class="trash" id="remove-address"><img
                                                src="/assets/svgs/Icon feather-trash-2.svg" alt=""></span>


                                </div>
                            </header>
                            <p>{{$address->city}} |{{$address->street}} | Building : {{$address->building}} | Floor
                                : {{$address->floor}}
                            </p>
                        </div>
{{--                        <input type="hidden" value="Save">--}}
                    </form>
                @endforeach

            </div>

        </div>
    @else
        <div>No Addresses Found</div>
    @endif

</main>