<div class=" content-padding collection " data-aos="fade-up">
    <div class="title" data-aos="fade-up">
        <div class="d-flex align-items-center">
            <h1 class="collection-title" data-aos="fade-up">
                {{$collection['label']}}
            </h1>
            {{--            @if(isset($collection['link']))--}}
            {{--            <a href="{{$collection['link']}}" class="underlined-link" data-aos="fade-up">--}}
            {{--                View All--}}
            {{--            </a>--}}
            {{--            @endif--}}
        </div>
    </div>

    @if(isset($collection['extension_image']))
        <div class="content-slide" data-aos="fade-up">
            <div class="content-image">
                <div class="asp cover asp-4-2">

                    <img src="{{env("DATA_URL")."/ecom_collections/".$collection['id'].".jpg"}}" alt="">
                </div>
            </div>
            <div class="card-width">

                <div class="product-content">
                    <div id="collection-carousel"
                         class="collection-carousel main-carousel owl-carousel product-carousel"
                         data-carousel-autowidth="false" data-carousel-items="1" data-carousel-autoplay="false"
                         data-carousel-loop="false" data-carousel-nav="true" data-carousel-dots="false"
                         data-carousel-nav-triggers="true" data-carousel-nav-next-id="" data-carousel-nav-prev-id=""
                         data-autoplay-timeout="10000">
                        @foreach($collection['products']['data'] as $product)
                            <div class="styling">
                                @component('pages.products.card',[ "product" => $product ]) @endcomponent
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="content-slide" data-aos="fade-up">
            <div class="card-width">

                <div class="product-content">
                    <div id="collection-carousel"
                         class="collection-carousel main-carousel owl-carousel product-carousel"
                         data-carousel-autowidth="false" data-carousel-items="1" data-carousel-autoplay="false"
                         data-carousel-loop="false" data-carousel-nav="true" data-carousel-dots="false"
                         data-carousel-nav-triggers="true" data-carousel-nav-next-id="" data-carousel-nav-prev-id=""
                         data-autoplay-timeout="10000">
                        @foreach($collection['products']['data'] as $product)
                            <div class="styling">
                                @component('pages.products.card',[ "product" => $product ]) @endcomponent
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    @endif

</div>

