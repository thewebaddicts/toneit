@extends('layouts.main')
<?php $cartList = ecom('cart')->getAsArray();

?>
<?php //try { echo request()->user->counts['cart']; } catch (\Throwable $th) { echo "0"; } ?>


<title>Tonit Daily | {{ __('title.cart') }}</title>
@section('content')
    <div class="topbanner">
        <figure>
            <img src="/assets/images/Mask Group 30.png" alt="">
        </figure>
        <figcaption>
            <span>Home / Cart</span>
            <h1>Cart</h1>


        </figcaption>
    </div>
    <section class="cart">
        <div class="content">
            <sep></sep>
            <sep></sep>

            @if($cartList['items'])
                @foreach($cartList['items'] as $item)
                    
                    <main class="responsive">
                        <div class="one">
                            <picture><img src="{{ $item['details']['thumb'] }}' alt="></picture>
                        </div>
                        <div class="two">
                            <div><h3>{{$item['details']['label']}}</h3>
                                {{--                                <h4>{{$item->details->small_description}}</h4>--}}
                                <span class="trash" data-target="remove-address-first"><img id="remove-address" onclick="removeItemFromCart({{$item['id']}})"src="/assets/svgs/Icon feather-trash-2.svg" alt=""></span>
                            </div>
                            <div class="price">
                                <label>Price</label>
                                <span>{{$item['details']['unit_price_formatted'] }}</span>
                            </div>

                            <span class="">
                                <div>
                                    <label>Quantity</label>
                <div class="quantity buttons_added">
                    <span>
                    <button type="button" @if($item['details']['stock_quantity'] < 1) disabled @endif class="minus"
                            onclick="@isset($item['id']) updateCartQuantity('{{$item['id']}}' , -1)@endif"><i
                                class="fas fa-minus"></i></button>
                    <input type="text" @if($item['details']['stock_quantity'] < 1) disabled @endif step="1" min="1"
                           name="quantity" value="{{$item['quantity']}}" title="Qty"
                           class="input-text qty text cart-count" size="4" pattern="" inputmode="">
                    <button type="button" @if($item['details']['stock_quantity']< 1) disabled @endif class="plus"
                            onclick="@isset($item['id']) updateCartQuantity('{{$item['id']}}' , 1) @endif"><i
                                class="fas fa-plus"></i></button>

                    </span>
                </div></div>
 </span>
                            <div>

                                <div class="subtotal"><label>SubTotal</label>
                                    {{--                                    <span>{{$item['total']['price']}}</span>--}}
                                </div>
                            </div>

                        </div>
                    </main>








                    <main>


                        <picture><img src="{{$item['details']['thumb']}}' alt="></picture>
                        <div style=" padding-right: 100px;"><h3>{{$item['details']['label']}}</h3>
                            {{--                            <h4>Classic Black</h4>--}}
                        </div>
                        {{--                        <span>{{$item['details']['unit_price_formatted'] }}</span>--}}
                        <span class="total">
                <div class="quantity buttons_added">
                    <span>
                    <button type="button" @if($item['details']['stock_quantity'] < 1) disabled @endif class="minus"
                            onclick="@isset($item['id']) updateCartQuantity('{{$item['id']}}' , -1)@endif"><i
                                class="fas fa-minus"></i></button>

                            <input type="text" @if($item['details']['stock_quantity']  < 1) disabled @endif step="1"
                                   min="1"
                                   name="quantity" value="{{$item['quantity']}}" title="Qty"
                                   class="input-text qty text cart-count" size="4" pattern="" inputmode=>

                        @if($item['quantity']<$item['details']['stock_quantity'])
                            <button type="button" @if($item['details']['stock_quantity'] < 1) disabled
                                    @endif class="plus"
                                    onclick="@isset($item['id']) updateCartQuantity('{{$item['id']}}' , 1) @endif"><i
                                        class="fas fa-plus"></i></button>
                        @else
                            <button type="button " disabled class="plus disabled"><i
                                        class="fas fa-plus"></i></button>
                        @endif
</span>

</div>
        <span class="discount">{{$item['details']['unit_price_beforediscount_formatted']}}</span>
<div>
{{--    7.36--}}
    {{--                <span>{{$item['total']['price']}}</span>--}}

<span>{{$item['details']['unit_price_formatted'] }}</span>

<span class="trash" data-target="remove-address-first"><img onclick="removeItemFromCart({{$item['id']}})"
                                                            src="/assets/svgs/Icon feather-trash-2.svg"
                                                            alt=""></span>  </div>
</span>
                        {{--<span></span>--}}
                    </main>
                @endforeach
                <sep></sep>
                <sep></sep>
                <div class="total-price">

                    @foreach($cartList['breakdown'] AS $breakdown)
                        <div>

                            <span>{{ $breakdown['label'] }}</span>

                            <span>{{ $breakdown['value_label'] }}</span>
                        </div>
                    @endforeach
                    <hr>
                    <div>
                        <span style="font-weight: bold">Total</span>
                        <span style="font-weight: bold">{{$cartList['total']['price']}}</span>

                    </div>
                </div>
                <div class="buttons">
                    <a href="/">Continue Shopping</a>
                       @if(session('login'))
                              <a href={{route('checkout')}}>Checkout</a>
                       @else
                              <a href="{{route('login',['customRouteName'=>'checkout'])}}">Checkout</a>
                       @endif



                </div>

        </div>
    </section>
@endsection
@else
    <div class="empty">
        <div>

            <h3>
                Your Cart is Empty</h3></div>
    </div>
@endif