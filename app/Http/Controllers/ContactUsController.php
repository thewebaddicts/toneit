<?php

namespace App\Http\Controllers;
use App\Mail\emailtemplate;
use App\Models\FooterCarouselModel;
use App\Models\ContactFormModel;
use App\Models\EmailTemplateModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public static function render()
    {
        $footercarousel= FooterCarouselModel::where('cancelled', 0)->get();
        return view('pages.contactus' , ['footercarousel'=>$footercarousel]);
    }

    public function store(Request $request){
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',


        ]);

        $form = new ContactFormModel();
        $form->full_name_contact = request()->input('first_name').' '.request()->input('last_name');
        $form->email_contact = request()->input('email');
        $form->subject_contact = request()->input('subject');
        $form->msg_contact = request()->input('message');
        $form->save();
        $template = EmailTemplateModel::where('cancelled', 0)->where('location', 'contact_corporate')->first();

        if ($template) {
            $dictionary = json_decode(json_encode([
                "title" => 'Contact form',
                "form_data" => [
                    "Full name" => $request->input('first_name').' '.$request->input('last_name'),
                    "Email" => $request->input('email'),
                    "Subject" => $request->input('subject'),

                ]
            ]));


            $company_email = explode(',', $template->email);

            foreach ($company_email as $email) {
                Mail::to(request()->input('email_contact'))->queue(new emailtemplate("contact_corporate_client", $dictionary));
                Mail::to($email)->send(new emailtemplate('contact_corporate', $dictionary));
            }
        }

        return  redirect()->route('contactus', ["notification_id" => 1015]);
    }
}
