<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class  HelpandFaqModel extends Model
{
    protected $table='help_faq';
}
