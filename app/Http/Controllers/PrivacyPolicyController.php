<?php

namespace App\Http\Controllers;
use App\Models\FooterCarouselModel;
use Illuminate\Http\Request;

class PrivacyPolicyController extends Controller
{
    public static function render()
    {
        

        $footercarousel = FooterCarouselModel::where('cancelled', 0)->get();
        return view('pages.privacypolicy', ['footercarousel' => $footercarousel]);
    }
}
