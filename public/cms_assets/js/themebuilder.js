
    function SaveMainForm(){
        if(!document.getElementById('main-form').reportValidity()){ 
            ShowToast('You are trying to submit invalid data in your form.'); 
        }else{ 
            document.getElementById('main-form').submit() 
        } 
    }

    function checkIfModalOpened(){
        if(isOpenedDrawer('element-options') == true){
                ShowToast("Please save or cancel before you switch to another component!","error");
                return false;
        }
        return true;
    }

    function OpenComponentList(){
        if(checkIfModalOpened()){ 
            toggleDrawer('sidebar-selected');
            $('#selected-components').addClass('d-none'); 
            $('#available-components').removeClass('d-none'); 
        }
    }

    function OpenSelectedComponentList(){
        if(checkIfModalOpened()){ 
            toggleDrawer('sidebar-selected');
            $('#selected-components').removeClass('d-none'); 
            $('#available-components').addClass('d-none'); 
        }
    }

    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    function ViewComponents(){
        $('#element-options').removeClass('visible');
        $('#sidebar-comp').toggleClass('visible');
    }

    function ViewElementOptions(elem){

       
        var id = elem.attr('data-component');
        var key = elem.attr('data-key');
       

        var payload = null;
        var hiddenInput = elem.find('input[name='+key+']');
        if(hiddenInput.length > 0){
            payload = hiddenInput.val();
        }

        if(isOpenedDrawer('element-options') == true){
            ShowToast("Please save or cancel before you switch to another component!","error");
            return;
        }

        openDrawer('element-options');

        $('#element-options .content-box').find('.loader').addClass('active');
        $.get(`/cms/theme-builder/component/${id}?${payload}`, {key : key} , function(data){
            $('#element-options .content-box').find('.inner').html(data);
            $('#element-options .content-box').find('.loader').removeClass('active');
            initializeComponents();
            initSortableList();
        });
    }

    function SavePayload(){
        var form = $('#element-options').find('form').serializeObject();
        var valid = true;

        $('#element-options form').find('input:not(.search)').each(function() {
            if ($(this).val() == '') {      
                $(this).addClass('invalid-form-element');
                valid = valid && false; 
            }else{
                $(this).removeClass('invalid-form-element');
                valid = valid && true;
            }
        });

        $('#element-options form').find('select').each(function() {
            if ($(this).hasClass('noselection')) {
                $(this).parent().addClass('invalid-form-element');
                valid = valid && false;
            }else{
                $(this).parent().removeClass('invalid-form-element');
                valid = valid && true;
            }
        });

        $('#element-options form').find('textarea').each(function() {
            if ($(this).val() == '') {
                $(this).addClass('invalid-form-element');
                valid = valid && false;
            }else{
                $(this).removeClass('invalid-form-element');
                valid = valid && true;
            }
        });

        if(valid == false){ ShowToast("Please fill all required fields!","error"); return; }
        
        var key = form.key;
        var value = $('#element-options').find('form').serialize();
        var box = $("[data-key = "+key+"]");

        if(box.find(`input[name=${key}]`).length == 0){
            var hiddenInput = `<input  name='${key}' value='${value}' type='hidden' class="to-save" />`; 
            box.append(hiddenInput);
        }else{
            box.find(`input[name=${key}]`).val(value);
        }

        box.addClass('filled');
        closeDrawer('element-options');
        openDrawer('sidebar-selected');

        SaveSettings();
    }

    function SaveSettings(){
        var  builder_id = $("#builder_id").val();

        if($('.element-custom:not(.filled)').length > 0){
            ShowToast("There are some components that are not filled","error"); return;
        }
      
        var result = [];
        
        $('#selected-components').find('.to-save').each(function(){
            result.push({
                component : $(this).parent().attr('data-component'),
                key : $(this).parent().attr('data-key'),
                value :  $(this).val(),
                label : $(this).parent().attr('data-label')
            }); 
        });

        // if(result.length == 0){ ShowToast("Please add components before you submit","error"); return; }

        $.post(`/cms/theme-builder/${builder_id}`, { result : result , _token : $("meta[name=csrf-token]").attr("content")}, function(){
                ShowToast("Successfully Saved!","success");
                RefreshPreview();
        });
    }

    function RefreshPreview(){
        var iframe = document.getElementById('FrameId');
        iframe.src = iframe.src;
    }

    function GenerateRandomString(length) {
        const characters ='abcdefghijklmnopqrstuvwxyz0123456789';
        let result = '';
        const charactersLength = characters.length;
        for ( let i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    function DeleteSelectedComponent(event,elem){
        event.stopPropagation();
        $('body').append('<div class="ui small basic test modal transition confirmation"><div class="ui icon header"><i class="fas fa-question-circle icon"></i>Are you sure?</div><div class="content center aligned"><p class="item text-center">Are you sure you want to delete the component ?</p></div><hr class="line white"><div class="actions"><div class="ui red basic cancel inverted button"><i class="remove icon"></i>No</div><div class="ui green ok inverted button"><i class="checkmark icon"></i>Yes</div></div></div>');
        $('.ui.modal.confirmation')
        .modal({
            closable  : false,
            onDeny    : function(){
            },
            onApprove : function() {
                if($('#selected-components').find('.element-custom').length == 1){
                    $('#selected-components').find('.inner').addClass('empty-val');
                }
                elem.parent().remove();
                SaveSettings();
            },
            onHidden: function(){
                $('.ui.modal.confirmation').remove();
            }
        })
        .modal('show'); 
    }

    function CreateComponentElement(key , componentID , componentLabel , filled){
        return `<div onclick="ViewElementOptions($(this))" class="ui segment ui-state-default element-custom position-rel ${filled == 1 ? 'filled' : ''}" data-label= "${componentLabel}" data-component = "${componentID}" data-key = "${key}">
            <button type="button" class="close-button" onclick="DeleteSelectedComponent(event , $(this))"> <i class="close icon"></i> </button>
            <div style="font-weight: 700;font-size: 14px;text-transform: uppercase;"> ${ componentLabel } <i class='msg' style='color: #ff0f0f'> (empty) </i> </div>    
        </div>`;
    }

    function AddElement(elem , componentID ){
        if($('.element-custom:not(.filled)').length > 0){ 
            ShowToast("Please setup your previously added component before you create a new one", "error");
            return;
        }
       
        $('#selected-components').find('.loader').addClass('active');
        
        var data = CreateComponentElement(GenerateRandomString(10) , componentID, elem.text() , 0);
       
        $('#selected-components').find('.inner').append(data);
        
        $('#selected-components').find('.loader').removeClass('active');
        $("#selected-components").find('.inner').removeClass('empty-val');
        $("#selected-components").removeClass('d-none'); 
        $('#available-components').addClass('d-none');


        ViewElementOptions($(data));

    }

    function initSortableList(){
        $( "#sortable-list" ).sortable({
            placeholder: "ui-state-highlight",
            update: function( event, ui ) {
                SaveSettings();
            }
        });
        $( "#sortable-list" ).disableSelection();
    }

    initSortableList();

