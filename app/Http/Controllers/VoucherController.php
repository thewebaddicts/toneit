<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class VoucherController extends Controller
{
    public function applyVoucher(){


        $code = request()->code;

        $cartList = ecom('cart')->applyVoucher($code);

        $breakdown = view('components.cart-breakdown', ['cartList'=> $cartList])->render();
        $return = [];

        $return['status'] = true;
        $return['breakdown'] = $breakdown;

        if(isset($cartList->error)){
            $return['message'] = $cartList->error["message"];
            $return['status'] = false;
        }

        if(isset($cartList->success)){
            $return['message'] = $cartList->success["message"];
            $return['status'] = true;
        }

        return response()->json( $return , 200);


    }

    public function removeVoucher(){

        $cartList = ecom('cart')->removeVoucher();

        $breakdown = view('components.cart-breakdown', ['cartList'=> $cartList])->render();
        $return = [];

        $return['status'] = true;
        $return['breakdown'] = $breakdown;

        if(isset($cartList->error)){
            $return['message'] = $cartList->error["message"];
            $return['status'] = false;
        }

        if(isset($cartList->success)){
            $return['message'] = $cartList->success["message"];
            $return['status'] = true;
        }



        return response()->json( $return , 200);
    }

}

