{{--@dd($product)--}}
<a href="{{route('product',[ "id" => $product['id'] , "slug" => $product['slug'] ])}}" class="productCard">

    @if($product['unit_price_beforediscount_formatted']>0)
        <span class="sale">
    <span class="number">
     {{$product['discount']}}%
    </span>

</span>
    @endif

    <img src="{{str_replace('?v=0','?v='.rand(0,100) ,$product['image'])}}" onerror="this.src='/assets/svgs/Logo.svg'"
         alt="">
    @isset($product['display_badge'])
        @if($product['display_badge'])
            <span style="display: block;display: block;
    position: absolute;
    top: 0;
    left: 0;"><span style="position: absolute;
    left: 50%;
    color: white;
    top: 40%;
    font-family: 'Restora';
    transform: translate(-50%,-50%);
    font-size: 15px;">New</span>
            <img src="{{ env('BASE_URL') }}/assets/svgs/Mask Group 45.svg" alt=""></span>
        @endif
    @endisset
    <a class="link" href="{{route('product',[ "id" => $product['id'], "slug" => Str::slug($product['label'])])}}">View
        More</a>
    <h3>{{ $product['label']}}</h3>
    {{--    <h3>{{ $product->small_description}}</h3>--}}
    <sep></sep>


    @isset($product['cms_attributes']['attributes_product_brand'])
        <span>{{$product['cms_attributes']['attributes_product_brand'] }}</span>
    @endisset


    @if($product['unit_price_beforediscount_formatted']>0)
        <span class="product-price discount">{{$product['unit_price_beforediscount_formatted']}}</span>
        <span class="product-price">{{$product['unit_price_formatted']}}</span>

    @else
        <span class="product-price">{{ $product['unit_price_formatted']}}</span>
    @endif
</a>
