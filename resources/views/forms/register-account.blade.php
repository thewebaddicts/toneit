<div class="create-account">
    <h2>Create an account</h2>

    <form method="POST" action="{{route('register')}}" name="loginform" id="loginform" class="section-form form">
        @csrf

        <div class="loginname">
            <div style="display: flex;flex-direction:column">
                <label for="">First Name</label>
                <input type="text" placeholder="Your First Name" name="first_name" required autocomplete>
            </div>
            <div style="display: flex;flex-direction:column"><label for="">Last Name</label>
                <input type="text" placeholder="Your Last Name" name="last_name" required autocomplete>
            </div>
        </div>
        <label for="email">E-mail</label>
        <input type="email" name="email" placeholder="Your E-mail" id="email" required autocomplete="">
        <label for="">Phone Number</label>

        <?php $profile = ecom('profile')->get();?>
        <?php  $PhoneCodes = ecom('countries')->getPhoneCodes();?>

        <div style="display: flex"><select name="phone_country_code" id="">
{{--                @foreach($codes AS $CountryCode)--}}
{{--                    <option @if($CountryCode['code'] === 'LB') selected--}}
{{--                            @endif value="{{ $CountryCode['code'] }}">--}}
{{--                        +{{ $CountryCode['phone_code'] }}--}}
{{--                    </option>--}}
{{--                @endforeach--}}
                @foreach($PhoneCodes AS $code)

                    <option value="{{ $code['code'] }}"
                            @if( $code['phone_code']=='961') selected @endif>{{ $code['phone_code'] }}</option>

                @endforeach
            </select>
            <input type="tel" placeholder="Your Phone Number" name="phone"></div>
        <label for="password">Password</label>
        <input type="password" minlength="6" placeholder="Password" id="pass1" name="password">
        <label for="password">Confirm Password</label>
        <input type="password"  minlength="6" placeholder="Confirm Password" name="confirm_password" id="pass2" onkeyup="checkPass(); return false;">
        <div id="error-nwl"></div>
        <input type="submit" value="Create">
        @if ($errors -> any())
            <ul>
                @foreach ($errors->all() as $item)
                    <li>{{$item}}</li>
                @endforeach


            </ul>

        @endif
    </form>
</div>
