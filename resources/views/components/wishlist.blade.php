

@php

    $wishlistproduct = ecom('favorites')->get() @endphp
<main id="wishlist">
    <h3>Wishlist</h3>
    <div>
@if(isset($wishlistproduct) && count( $wishlistproduct)>0)

    @foreach( $wishlistproduct AS $product)

                @php
                    if( ecom('users')->loginCheck()){

                         $favorite_active = ecom('favorites')->check($product['id']);

                     }else{
                         $favorite_active = false;
                     }
                @endphp
        <div>


{{--            <div style="    display: flex;justify-content: flex-end;" data-pid="{{$product->id}}"--}}
{{--                 class="favorite-toggle @if($favorite_active) favorite-toggle-active @endif"--}}
{{--                 href="javascript:;">--}}
{{--                                <span onclick="ToggleFavoritesInit()">Add to wishlist<i class="far fa-heart"></i> <i--}}
{{--                                            class="fas fa-heart" id="myImg" style="color: #998484"></i></span>--}}
{{--            </div>--}}




            <picture>

                <img src="{{$product['image']}}"  onerror="this.src='/assets/svgs/Logo.svg'" alt="">

{{--                <img src={{$product['image']}} alt="">--}}

                <a href="product/{{$product['id']}}/{{$product['slug']}}">View More</a>
{{--                <i onclick="ToggleFavoritesInit()" class="fas fa-heart favorite-toggle" data-pid="{{$product['id']}}" id="heart"></i>--}}
            </picture>

            <h3>{{$product['label']}}</h3>
{{--            <h4>Maybelline</h4>--}}
            <span>{{$product['unit_price_formatted']}}</span>


        </div>

            @endforeach
    @else
    <div>No Products</div>
    @endif
    </div>


</main>
