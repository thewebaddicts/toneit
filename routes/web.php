<?php

use App\Http\Controllers\AboutUsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\NewsPagecontroller;
use App\Http\Controllers\DetailsPageController;
use App\Http\Controllers\CookiesController;
use App\Http\Controllers\PrivacyPolicyController;
use App\Http\Controllers\TermsandConditionsController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\BrandsController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\VoucherController;
use App\Http\Controllers\CartController;
use App\Models\BrandsModel;
use App\Models\SlideshowModel;
use twa\ecomauth\facades\EcomUsersRegistrationFacade;
use twa\ecomgeneral\helpers\cartHelper;
use twa\ecomprofile\facades\EcomAddressesFacade;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(["middleware" => [\App\Http\Middleware\SEO::class]], function () {
    Route::middleware(twa\ecomgeneral\middleware\storeDefaultRouteMiddleware::class)->get('/', function () {
//   return view('pages.corporate.default');
    });

    Route::group([
        'prefix' => '/{store_prefix}/{language_prefix}/',
        'middleware' => [twa\ecomgeneral\middleware\storeRoutesMiddleware::class, twa\ecomgeneral\middleware\storeDefaultDataMiddleware::class],
    ],
        function () {
            Route::get('/mili', function () {

                \twa\ecomproducts\controllers\EcomProductsSearchController::generateJson();

            });
            Route::get('/', [HomepageController::class, 'render'])->name('home');
//    Route::get('/page',  [PageController::class, 'render'])->name('page');
            Route::get('/news', [NewsPagecontroller::class, 'render'])->name('news');
            Route::get('/helpandfaq/{id}/', [\App\Http\Controllers\HelpandFaq::class, 'render'])->name('helpandfaq');
            Route::get('/product/{id}/{slug}',
                function ($store_prefix, $lang, $id, $slug) {

                    return ecom('products')->condition('productID', $id)->condition('display', "full")->get()->render();
                })->name('product');

//authentication
            Route::get('/forgot', function () {
                return view('pages.forgot');
            })->name('forgot');
            Route::post('/forgot', [EcomUsersRegistrationFacade::class, 'forgot'])->name('forgot-password');
            Route::get('/register', function () {
                if (session('login'))
                    return redirect()->route('account');
                return view('pages.login');
            })->name('register');
            Route::post('/register', [EcomUsersRegistrationFacade::class, 'store'])->name('register');

            Route::get('/login', function () {
                if (session('login'))
                    return redirect()->route('account');
                return view('pages.login');
            })->name('login');

            Route::post('/newsletter', [\App\Http\Controllers\HomeController::class, 'subscribeEcom'])->name('newsletter');
            Route::post('/profile/edit', [\App\Http\Controllers\UsersController::class, 'updateProfile'])->name('profile-edit');
            Route::get('/profile/edit', [\App\Http\Controllers\UsersController::class, 'updateProfile'])->name('account');

            Route::post('/login',function (){ return ecom('users')->processLogin();})->name('login');
            Route::get('/logout', [EcomUsersRegistrationFacade::class, 'logout'])->name('logout');
//search
            Route::post('/voucher/apply', [VoucherController::class, 'applyVoucher'])->name('apply-voucher');
            Route::post('/voucher/remove', [VoucherController::class, 'removeVoucher']);
            Route::get('/search', function ($store, $lang) {

                return view('pages.page', ["products" => ecom('products')->includeFilters()->addFilters(request()->input('filter', []))->setMinPrice(request()->input('filter_price_min'))->setMaxPrice(request()->input('filter_price_max'))->ByTerm(request()->input('term'))]);
            })->name('search');

//add address ecom
            Route::group(['middleware' => [twa\ecomauth\middleware\AuthenticationRequiredCheck::class]], function () {

                Route::get('/account/addresses/delete/{id}', [EcomAddressesFacade::class, 'deleteAddress'])->name('delete-address');
                Route::get('/account-add-address ', [EcomAddressesFacade::class, 'renderAdd'])->name('account-addresses');
                Route::get('/account-address', [EcomAddressesFacade::class, 'renderAdd']);
                Route::post('/account-add-address/add', [EcomAddressesFacade::class, 'saveAddress'])->name('create-address');
                Route::get('/account/addresses/add', [EcomAddressesFacade::class, 'renderAdd'])->name('account-addresses');

                Route::get('/change/password', [\App\Http\Controllers\UsersController::class, 'changePassword'])->name('account');
                Route::post('/change/password', [\App\Http\Controllers\UsersController::class, 'changePassword'])->name('changePassword');
                //my address

                Route::get('/account', [AccountController::class, 'accountPage'])->name('account');
                Route::get('/account-address', [AccountController::class, 'render'])->name('account-address');
                Route::get('/account-wishlist', [AccountController::class, 'accountWishlist'])->name('account-wishlist');
                Route::get('/account-order', [AccountController::class, 'accountOrder'])->name('account-order');
                Route::get('/account-order-details/{id}', [AccountController::class, 'accountOrderDetails'])->name('account-order-details');
                Route::get('/account-add-address', [AccountController::class, 'accountaddAddress'])->name('account-add-address');
            });
            //products
            // Socialite
            Route::get('/login/google', function () {
                return ecom('users')->SocialLogin('google');
            })->name('login.google');
            Route::get('/login/facebook', function () {
                return ecom('users')->SocialLogin('facebook');
            })->name('login.facebook');

            Route::get('/products-collection/{slug}', function ($store_prefix, $lang, $id, $slug) {
                return view('pages.page',
                    ['collections' => ecom('collections')->condition('display_homepage', 1)->condition('id', $id)->condition('IncludeProducts', 1)->get()
                        , 'products' => ecom('products')->condition('display', "card")->condition('cancelled', 0)->includeFilters()->addFilters(request()->input('filter', []))->get()->byCollectionOrMenuSlug($slug)]);
            })->name('products-collection');

            Route::post('/products/{slug}', function ($store_prefix, $lang, $slug) {
                return view('components.product-listing', ['products' => ecom('products')->condition('display', "card")->includeFilters()->addFilters(request()->input('filter', []))->byCollectionOrMenuSlug($slug)]);
            })->name('products');


            //category route


            Route::get('/products/subcategory/{slug}', function ($store_prefix, $lang, $slug) {
                $products = ecom('products')->condition('display', "card")->condition('ignoreListing', 1)->condition('display', 1)->condition('cancelled', 0)->includeFilters()->addFilters(request()->input('filter', []))->get()->byCollectionOrMenuSlug($slug);

                return view('pages.page', ['products' => $products]);
            });


            Route::get('/account/addresses/cities/list', [EcomAddressesFacade::class, 'renderCities'])->name('address_cities');
            Route::get('/account/addresses/states/list', [EcomAddressesFacade::class, 'renderStates'])->name('address_states');

//    Route::get('/product/{id}/{slug}', [ProductController::class, 'render'])->name('product');


            Route::get('/products/brand/{id}/{slug}', function ($store_prefix, $lang, $id, $slug) {

                $products = ecom('products')->condition('cancelled', 0)->condition('display', "card")->condition("extraFields", "display_badge")->includeFilters()->addFilter($id)->addFilters(request()->input('filter', []))->get()->toArray();


                return view('pages.page',
                    ['products' => $products,
                        'brands' => $brands_letter = BrandsModel::where('cancelled', 0)->get(),
                    ]);
            });


            Route::get('/products/{slug}', function ($store_prefix, $lang, $slug) {


                if (request()->sortBy == 'alphaA') {
                    return view('pages.page',
                        ['products' => ecom('products')->condition('order', 'label_asc')->condition('display', "card")->condition("extraFields", "display_badge")->includeFilters()->addFilters(request()->input('filter', []))->byCollectionOrMenuSlug($slug),
                            'brands' => $brands_letter = BrandsModel::where('cancelled', 0)->get(),
                        ]);
                }
                if (request()->sortBy == 'alphaD') {
                    return view('pages.page',
                        ['products' => ecom('products')->condition('order', 'label_desc')->condition('display', "card")->condition("extraFields", "display_badge")->includeFilters()->addFilters(request()->input('filter', []))->byCollectionOrMenuSlug($slug),
                            'brands' => $brands_letter = BrandsModel::where('cancelled', 0)->orderBy('id', 'DESC')->get(),
                        ]);
                }
                if (request()->sortBy == 'priceA') {
                    return view('pages.page',
                        ['products' => ecom('products')->condition('order', 'price_asc')->condition('display', "card")->condition("extraFields", "display_badge")->includeFilters()->addFilters(request()->input('filter', []))->byCollectionOrMenuSlug($slug),
                            'brands' => $brands_letter = BrandsModel::where('cancelled', 0)->get(),
                        ]);
                }
                if (request()->sortBy == 'priceD') {

                    return view('pages.page',
                        ['products' => ecom('products')->condition('order', 'price_desc')->condition('display', "card")->condition("extraFields", "display_badge")->includeFilters()->addFilters(request()->input('filter', [])),
                            'brands' => $brands_letter = BrandsModel::where('cancelled', 0)->get(),
                        ]);
                }
//        if (request()->sortBy == 'discount') {
//            return view('pages.page',
//                ['products' => ecom('products')->condition('display', "card")->includeFilters()->addFilters(request()->input('filter', []))->ByMenuQuery($id),
//                    'brands' => $brands_letter = BrandsModel::where('cancelled', 0)->get(),
//                ]);
//        }

                return view('pages.page', ['products' => ecom('products')->condition('display', "card")->condition('cancelled', 0)->condition("extraFields", "display_badge")->includeFilters()->addFilters(request()->input('filter', []))->byCollectionOrMenuSlug($slug),
                    'brands' => $brands_letter = BrandsModel::where('cancelled', 0)->get(),]);
            });


            Route::group(['middleware' => [twa\ecomgeneral\middleware\storeDefaultDataMiddleware::class], 'prefix' => '/cart'], function () {
                Route::get('/', function () {
                    return ecom('cart')->render();
                })->name('cart');
                Route::post('/update-quantity', [CartController::class, 'updateQuantity']);
                Route::post('/remove', [CartController::class, 'removeItem']);
                Route::get('/dropdown', function () {
                    return view('iframes.cart');
                })->name('cart-dropdown');
                Route::get('/get-summary', [CartController::class, 'getCartSummary']);
                Route::get('/get-details', [CartController::class, 'getCartDetails']);

                Route::get('/checkout', [\App\Http\Controllers\CheckoutController::class, 'checkout'])->name('checkout');
                Route::get('/checkout-payment', [\App\Http\Controllers\CheckoutController::class, 'checkoutpayment'])->name('checkout-payment');

                Route::get('/checkout-order', [\App\Http\Controllers\CheckoutController::class, 'checkoutordersummary'])->name('checkout-order');
                Route::post('/checkout-order', function () {
                    return (new \twa\ecomgeneral\helpers\purchasesHelper)->placeOrder();
                })->name('checkout-placeorder');
            });

            Route::get('/article/{id}/{slug}', [DetailsPageController::class, 'render'])->name('article');
            Route::get('/termsandconditions', [TermsandConditionsController::class, 'render'])->name('termsandconditions');
            Route::get('/privacypolicy', [PrivacyPolicyController::class, 'render'])->name('privacypolicy');
            Route::get('/cookies', [CookiesController::class, 'render'])->name('cookies');
            Route::get('aboutus', [AboutUsController::class, 'render'])->name('aboutus');

            Route::get('/brands', [BrandsController::class, 'render'])->name('brands');


//            Route::get('/contactus', [ContactUsController::class, 'render'])->name('contactus');
            Route::post('/contact-us', [ContactUsController::class, 'store'])->name('contact-us');
//    Route::get('/cart', [CartPageController::class, 'render'])->name('cart');


        });

});