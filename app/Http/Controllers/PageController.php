<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FooterCarouselModel;
use App\Models\PageProductModel;

class PageController extends Controller
{
    public static function render()
    {

        $productspage = PageProductModel::where('cancelled', 0)->paginate(1);
        $productsCount  = PageProductModel::where('cancelled', 0)->count();
        $footercarousel = FooterCarouselModel::where('cancelled', 0)->get();
        return view('pages.page', ['footercarousel' => $footercarousel , 'productsCount' => $productsCount,'productspage'=>$productspage]);
        
    }
}
