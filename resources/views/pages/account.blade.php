@extends('layouts.main')
@section('content')
    @php
        $profile = ecom('profile')->get();
        $addresses = ecom('addresses')->list();

    @endphp

    <div class="topbanner">
        <figure>
            <img src="/assets/images/Mask Group 30.png" alt="">
        </figure>
        <figcaption>
            <span>Home / Account</span>
            <h1>Account</h1>


        </figcaption>
    </div>
    <section class="account-info">
        <div class="content">
            <div>
@component('components.account-sidebar')@endcomponent





                <?php  $PhoneCodes = ecom('countries')->getPhoneCodes();
                ?>

                <main id="first" class="active">
                    <h3>Account Details</h3>

                    <form style="display: contents" method="POST" action="{{route('profile-edit')}}">
                        @csrf
                        <div class="sub-form">
                            <div>
                                <label for="">First Name</label>
                                <input type="text" name="first_name" value="{{$profile->first_name}} ">
                            </div>

                            <div>
                                <label for="">Last Name</label>
                                <input type="text" name="last_name" value="{{$profile->last_name}}">
                            </div>
                            <div>
                                <label for="">E-mail</label>
                                <input type="email" disabled value="{{$profile->email}}">
                            </div>

                            <div>
                                <label for="">Phone Number</label>
                                <div style="display: flex">
                                    @php
                                        $codes1 = getter('countries')->condition('code',$profile->phone_country_code)->get();



                                    @endphp



@isset($codes1[0]->phone_code)
<input type="number" value="{{$codes1[0]->phone_code}}" disabled>


                                    <input type="hidden" name="phone_country_code" value="{{$profile->phone_country_code}}">
                                    <input name="phone" type="tel" value="{{$profile->phone_original}}">
                                </div>
                            </div>
                            <input type="submit" value="Save">
                        </div>
                    </form>

                    <h3>Change Password</h3>

                    <form method="post" action={{route('changePassword')}}>
                        @csrf
                        <div class="sub-form">
                            <div>
                                <label for="">Current Password</label>
                                <input type="password" name="current_password">
                            </div>

                            <div>

                                <input type="hidden">
                            </div>
                            <div>
                                <label for="">New Password</label>
                                <input type="password" name="password">
                            </div>
                            <div>
                                <label for="">Confirm Password</label>
                                <input type="password" name="confirm_password">
                            </div>
                            <input type="submit" value="Save">
                        </div>
                    </form>
                </main>
            </div>

        </div>
        @endisset

    </section>
@endsection
