<?php

return [
    'success' => [
        "action" => "redirect", //can be either redirect or display
        "redirect" => "https://google.com/?success",
        "function" => "myfunction()" //this function is executed upon success
    ],
    'fail' => [
        "action" => "redirect", //can be either redirect or display
        "redirect" => "https://google.com/?success",
    ],
    'default_aramex' => [
        "provider" => "aramex",
        "data" => ["ARAMEX_ACCOUNT_ENTITY" => "BEY", "ARAMEX_ACCOUNT_NUMBER" => "60513206", "ARAMEX_ACCOUNT_PIN" => "665165", "ARAMEX_ACCOUNT_USERNAME" => "info@tonitdaily.com", "ARAMEX_ACCOUNT_PASSWORD" => "Tone'it@2021?", "ARAMEX_ACCOUNT_FULLNAME" => "Supplima SARL", "ARAMEX_ACCOUNT_COMPANY" => "Supplima SARL", "ARAMEX_ACCOUNT_PHONE" => "0096170786038", "ARAMEX_ACCOUNT_MOBILE" => "0096170786038", "ARAMEX_ACCOUNT_FAX" => "0096170786038", "ARAMEX_ACCOUNT_EMAIL" => "info@tonitdaily.com", "ARAMEX_ACCOUNT_ZIPCODE" => "0000", "ARAMEX_ACCOUNT_ADDRESS" => "Antelias", "ARAMEX_ACCOUNT_CITY" => "Beirut", "ARAMEX_COUNTRY_CODE" => "LB"]
    ],
    'aramex' => [
        "provider" => "aramex",
        "data" => ["ARAMEX_ACCOUNT_ENTITY" => "BEY", "ARAMEX_ACCOUNT_NUMBER" => "60513206", "ARAMEX_ACCOUNT_PIN" => "665165", "ARAMEX_ACCOUNT_USERNAME" => "info@tonitdaily.com", "ARAMEX_ACCOUNT_PASSWORD" => "Tone'it@2021?", "ARAMEX_ACCOUNT_FULLNAME" => "Supplima SARL", "ARAMEX_ACCOUNT_COMPANY" => "Supplima SARL", "ARAMEX_ACCOUNT_PHONE" => "0096170786038", "ARAMEX_ACCOUNT_MOBILE" => "0096170786038", "ARAMEX_ACCOUNT_FAX" => "0096170786038", "ARAMEX_ACCOUNT_EMAIL" => "info@tonitdaily.com", "ARAMEX_ACCOUNT_ZIPCODE" => "0000", "ARAMEX_ACCOUNT_ADDRESS" => "Antelias", "ARAMEX_ACCOUNT_CITY" => "Beirut", "ARAMEX_COUNTRY_CODE" => "LB"]
    ],
];