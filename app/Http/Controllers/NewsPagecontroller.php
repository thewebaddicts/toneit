<?php

namespace App\Http\Controllers;
use App\Models\NewsPageModel;
use Illuminate\Http\Request;

class NewsPagecontroller extends Controller
{
public static function render()
{
$news = NewsPageModel::where('cancelled', 0)->paginate(4);
if (request()->sortBy == 'dateasc') {
$news = NewsPageModel::where('cancelled', 0)->orderBy('date', 'asc')->paginate(4);

return view('pages.news', ['news' => $news]);
}
if (request()->sortBy == 'datedesc') {
$news = NewsPageModel::where('cancelled', 0)->orderBy('date', 'desc')->paginate(4);

return view('pages.news', ['news' => $news]);
}
return view('pages.news', ['news' => $news]);
}
}