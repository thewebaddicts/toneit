
@foreach($cartList['breakdown'] as $breakdown)
<div class="total-price" style="height: 40px;">

    <div style="height: 100%">

        <span>{{$breakdown['label']}}</span>
        <span>  {{$breakdown['value_label']}}</span>
    </div>

</div>
@endforeach
<div class="coupon-code">
    {{--                    <form method="post" name="voucherForm" id="voucherForm" action="{{route('apply-voucher')}}"--}}
    {{--                          class="coupon-form" onsubmit="return false" style="display: contents">--}}
    {{--                        @csrf--}}
    @php
        $code = isset($cartList->voucher_code) ? $cartList->voucher_code : "";
$code_value = "";
$valid = false;

if(isset($code) && !empty($code)){
$code_value = $code;
$valid = true;
}

    @endphp

    <form method="post" name="voucherForm" id="voucherForm" action="{{route('apply-voucher')}}" class="coupon-form"
          onsubmit="return false">
        @csrf
        <div>
            <div>
                <input type="text" placeholder="Promo Code"
                       onkeyup="if (event.key === 'Enter' || event.keyCode === 13) { applyVoucher($(this).next('input'));}"
                       id="coupon-control" name="promo_code">
                <input class="checkout-btn" type="submit" @if($valid) value="Remove" @else  value="Apply" @endif
                       data-apply-text="Apply"
                       data-remove-text="Remove" data-type="@if($valid){{'remove'}}@endif"
                       onclick="applyVoucher($(this))">

                {{--                                    <button type="button" class="btn-no-border checkout-btn" data-apply-text="Apply"--}}
                {{--                                            data-remove-text="Remove" data-type="@if($valid){{'remove'}}@endif"--}}
                {{--                                            onclick="applyVoucher($(this))">--}}
                {{--                                        @if($valid) <span class="btn-text"> Remove </span> @else <span class="btn-text">--}}
                {{--                    Apply </span> @endif--}}
                {{--                                    </button>--}}

            </div>

{{--            <span>-&nbsp;{{$cartList['items']}}</span>--}}
        </div>
    </form>


    {{--    <div class="checkout-coupon">--}}
    {{--        <form method="post" name="voucherForm" id="voucherForm" action="{{route('apply-voucher')}}" class="coupon-form" onsubmit="return false">--}}
    {{--            @csrf--}}

    {{--            <div class="coupon-card">--}}
    {{--                <input type="text" value="" onkeyup=" if (event.key === 'Enter' || event.keyCode === 13) { applyVoucher($(this).next('button'));}" placeholder="Coupon Code" class="text-input" id="coupon-control" name="promo_code" >--}}

    {{--                <button type="button" class="btn-no-border checkout-btn" data-apply-text="Apply"--}}
    {{--                        data-remove-text="Remove"  data-type="@if($valid){{'remove'}}@endif" onclick="applyVoucher($(this))">--}}
    {{--                    @if($valid) <span class="btn-text"> Remove </span> @else <span class="btn-text">--}}
    {{--                Apply </span> @endif--}}
    {{--                </button>--}}

    {{--            </div>--}}
    {{--        </form>--}}
    {{--    </div>--}}


</div>
<div class="total-price" style="height: 40px;">
    <div style="height: 100%"  >
        <span>Grand Total</span>

        <span>{{$cartList['total']['price']}}</span>
    </div>
</div>