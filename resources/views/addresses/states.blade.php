<?php
use Illuminate\Support\Facades\Http;

?>

<div class="form-group">
    <i class="fas fa-spinner fa-spin state-loader d-none"></i>
    <label class="t-1-semibold d-block mb-2">{{__('State')}}</label>
    <select name="state" id="state"  class="@error('state') is-invalid @enderror text-field with-border" onchange="changeCity()">
        <option value="" selected="" disabled="">State</option>
        @if($States ?? ''==0)<option value=""disabled="">Please Choose your country first</option>@endif
        @foreach($States ?? '' AS $State)
            <option value="{{ $State->Name }}" >{{ $State->Name }}</option>
        @endforeach
    </select>
    @error('state')<span class="invalid-feedback" role="alert"><strong>{{$message}}</strong></span>@enderror
</div>
