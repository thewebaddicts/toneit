<?php

namespace App\Http\Controllers;

use App\Models\FooterCarouselModel;
use App\Models\LoginModel;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public static function render()
    {

        $footercarousel = FooterCarouselModel::where('cancelled', 0)->get();
        return view('pages.login', ['footercarousel' => $footercarousel]);
    }
    
    public static function insert()
    {
        $validated = request()->validate([
            'name'=>'required|max:100'
        ]);
        $test = new LoginModel;
        $test->name = request()->firstname;
        $test->lastname = request()->lastname;
        $test->phonenumber = request()->lastname;
        $test->email = request()->email;
        $test->password = request()->password;
        $test->save();
    }
}
