@extends('layouts.main')

<?php $cartList = ecom('cart')->getAsObject();

?>

@section('content')
    <?php
    $collections = ecom('collections')->condition('display_homepage', 1)->condition('IncludeProducts', 1)->get()

    ?>

    @php

        if(ecom('users')->loginCheck()){

                 $favorite_active = ecom('favorites')->check($product['id']);

             }else{
                 $favorite_active = false;
             }

    @endphp

    <section class="product-details">
        <div class="content">
            <h1>Home&nbsp;/&nbsp;{{$product['label']}}</h1>
            <div>

                <div class="images">


                    <div>

                        @php

                            $gallery = json_decode(json_encode($product['gallery']))
                        @endphp

                        @foreach ($gallery as $galleries)

                            <picture class="{{$loop->first ? 'active' : ''}}">
                                <a data-fancybox="gallery" href="{{$galleries->thumb}}">
                                    <img src="{{ $galleries->image}}" alt="">
                                </a>
                            </picture>

                        @endforeach
                    </div>
                    <picture>

                        @if($product['stock_quantity'] == 0)
                            <span>Sold out</span>
                        @endif
                        <a data-fancybox="gallery" href="{{$product['image']}}"><img id="expandedImg"
                                                                                     src="{{ $product['image']}}"
                                                                                     onerror="this.src='/assets/svgs/Logo.svg'"
                                                                                     alt="">
                        </a>
                    </picture>


                </div>

                <div class="info">

                    <header>
                        <h1>{{$product['label']}}</h1>
                        @if(session("login"))
                            <div style="    display: flex;justify-content: flex-end;width: 175px;"
                                 data-pid="{{$product['id']}}"

                                 class="favorite-toggle @if($favorite_active) favorite-toggle-active @endif"
                                 href="javascript:;">
                                <span onclick="ToggleFavoritesInit()"><i
                                            class="far @if($favorite_active)fas @endif fa-heart"></i></span>
                            </div>
                        @endif

                    </header>

                    <h2>{{$product['small_description_en']}}</h2>
                    @if($product['unit_price_beforediscount_formatted']>0)
                        <span class="discount">{{$product['unit_price_beforediscount_formatted']}}</span>
                        <span>{{$product['unit_price_formatted']}}</span>

                    @else
                        <span>{{$product['unit_price_formatted']}}</span>
                    @endif


                    @php
                        $product_fam_id = $product['family_group_id'];
                            $products = getter('ecom_products')->condition('family_group_id' , $product_fam_id)->get();

                                $primary_variation = getter('ecom_products_variation_primaries')->get();

                    @endphp
                    <div class="variation">

                        @foreach($products as $pro)

                            @foreach($primary_variation as $primary)

                                @if($primary->id == $pro->ecom_products_variation_primary_id)

                                    <a style="display:contents;" class="link"
                                       href="{{route('product',[ "id" => $pro->id , "slug" => Str::slug($pro->label)])}}">
                                        <span>
                                            <img src={{env('DATA_URL')}}/variation_primaries/{{$primary->id}}.{{$primary->extension_image}}>
                                            @if($pro->id == $product['id'])
                                                <i style="margin-left: 40%;" class="fas fa-angle-up"></i>
                                            @endif
                                        </span>
                                    </a>
                                @endif
                            @endforeach

                        @endforeach
                    </div>

                    <span class="keyword">{{$product['variation_primary_label']}}</span>
                    <div class="quantity">
                        <form style="display: contents" id="mainform">
                            <span>Quantity</span>


                            <div class="quantity-grid">
                                  <span style="cursor: pointer" onmousedown="mouseDown_d()"
                                                                  onmouseup="mouseUp()"
                                                                  onmouseleave="mouseLeave()">-</span>

                                <input type="hidden" name="product_id" value={{$product['id']}}>
                                <span> <input id="xxxx" name="quantity" type="text" value="1" min="1"></span>

                                <span style="cursor: pointer"  onmousedown="mouseDown_i({{$product['stock_quantity']}})" onmouseup="mouseUp()" onmouseleave="mouseLeave()">+</span>
                            </div>


                            <button type="button" @if($product['stock_quantity'] == 0) disabled
                                    style="cursor: not-allowed;background: #5050508a;" @else style="cursor: pointer"
                                    @endif
                                    onclick="addToCart($('#mainform').serialize()); return false;">Add to Cart
                            </button>

                        </form>
                    </div>

                    @isset($product['tab_1_content'])
                        @if($product['tab_1_display'] == 1)
                            <li class="accordion">
                                <a style="cursor: pointer"><span><img src="/assets/svgs/Icon ionic-ios-arrow-back.svg"
                                                                      alt=""></span><span>{{$product['tab_1_title']}}</span></a>
                                <ul class="expandable sublist">

                                    <li>
                                        {!!nl2br($product['tab_1_content'])!!}
                                    </li>
                                </ul>
                            </li>
                        @endif
                    @endisset
                    @isset($product['tab_2_content'])
                        @if($product['tab_2_display'] == 1)
                            <li class="accordion">
                                <a style="cursor: pointer"><span><img src="/assets/svgs/Icon ionic-ios-arrow-back.svg"
                                                                      alt=""></span><span>{{$product['tab_2_title']}}</span></a>
                                <ul class="expandable sublist">

                                    <li>
                                        {!!nl2br($product['tab_2_content'])!!}
                                    </li>
                                </ul>
                            </li>
                        @endif
                    @endisset
                    @isset($product['tab_3_content'])
                        @if($product['tab_3_display'] == 1)
                            <li class="accordion">
                                <a style="cursor: pointer"><span><img src="/assets/svgs/Icon ionic-ios-arrow-back.svg"
                                                                      alt=""></span><span>{{$product['tab_3_title']}}</span></a>
                                <ul class="expandable sublist">

                                    <li>
                                        {!!nl2br($product['tab_3_content'])!!}
                                    </li>
                                </ul>
                            </li>
                        @endif
                    @endisset
                    @isset($product['tab_4_content'])
                        @if($product['tab_4_display'] == 1)
                            <li class="accordion">
                                <a style="cursor: pointer"><span><img src="/assets/svgs/Icon ionic-ios-arrow-back.svg"
                                                                      alt=""></span><span>{{$product['tab_4_title']}}</span></a>
                                <ul class="expandable sublist">

                                    <li>
                                        {!!nl2br($product['tab_4_content'])!!}
                                    </li>
                                </ul>
                            </li>
                        @endif
                    @endisset


                </div>
            </div>
        </div>
    </section>








    @if($product['recommendations']['also_bought'] !== false)

        <section class="recommended">
            <div class="content">
                <h1>Also Bought</h1>
                <h2></h2>
                <div class="owl-carousel carousel multi-carousel" data-carousel-items="5" data-carousel-dots="false"
                     data-carousel-loop="true" data-carousel-nav="true" data-carousel-autoplay="true"
                     data-carousel-autowidth="true">

                    @foreach($product['recommendations']['also_bought'] as $products)

                        <figure>
                            <a href="{{route('product',[ "id" => $products['id'] , "slug" => Str::slug($products['label']) ])}}">
                                <img src="{{ env('DATA_URL') }}/products/{{ $products['id']}}.{{ $products['extension_image']}}?v={{ $products['version']}}' alt="
                                     alt="">
                                <figcaption>
                                    <h3>{{ $products['label']}}</h3>


                                    <span>{{ $products['unit_price_formatted']}}</span>
                                </figcaption>
                            </a>
                        </figure>

                    @endforeach

                </div>
            </div>
        </section>

    @endif

    @if($product['recommendations']['recommended'] !== false)

        <section class="recommended">
            <div class="content">
                <h1>Recommended Products</h1>
                <h2></h2>
                <div class="owl-carousel carousel multi-carousel" data-carousel-items="5" data-carousel-dots="false"
                     data-carousel-loop="true" data-carousel-nav="true" data-carousel-autoplay="true"
                     data-carousel-autowidth="true">

                    @foreach($product['recommendations']['recommended'] as $products)

                        <figure>
                            <a href="{{route('product',[ "id" => $products['id'] , "slug" => Str::slug($products['label']) ])}}">
                                <img src="{{ env('DATA_URL') }}/products/{{ $products['id']}}.{{ $products['extension_image']}}?v={{ $products['version']}}' alt="
                                     alt="">
                                <figcaption>
                                    <h3>{{ $products['label']}}</h3>


                                    <span>{{ $products['unit_price_formatted']}}</span>
                                </figcaption>
                            </a>
                        </figure>

                    @endforeach

                </div>
            </div>
        </section>

    @endif

@endsection

@section('script')
    <script>
        FooterFunctions()
        {
            ToggleFavoritesInit();
        }
    </script>

@endsection