<div class="topbanner">
	<figure>
		<img src="/assets/images/Mask Group 30.png" alt="">
	</figure>
	<figcaption>
	
		@if(isset($products['menu']->label))
			<span>Home / {{$products['menu']->label}}</span>
			<h1>{{$products['menu']->label}}</h1>
			{{--        <h2> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</h2>--}}
		@else
			<span>Home / {{request()->input('term')}}</span>
			<h1>{{request()->input('term')}}</h1>
		@endif
	</figcaption>
</div>
