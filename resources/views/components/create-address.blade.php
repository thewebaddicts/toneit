@php

    $profile= ecom('profile')->get();
$Check = ecom('countries')->getCountries();
try{ $Countries = json_decode($Check); }catch (Throwable $e){ $Countries = []; }

@endphp

<main id="edit-address-first">

    <h3>Addresses</h3>
    <div>
    
        <input type="hidden" value="{{request()->input('redirect',NULL)}}" name="redirect">
        <form action="{{route('create-address')}}" class="address-form" method="post">
            @csrf
            
            <div class="form-container">
                <div class="form-title">
                    <h4>Address 1</h4>
                </div>
                <div class="form-content">
                    <div class="input-div">
                        <label>Address Title</label>
                        <input class="text-input" type="text" name="label" id="" placeholder="Address Title" required>
                    </div>
                    <div class="input-div">
                        <label>First Name</label>
                        <input type="text" name="first_name" value="{{$profile->first_name}}" required>

                    </div>
                    <div class="input-div">
                        <label>Last Name</label>
                        <input type="text" name="last_name" value="{{$profile->last_name}}" required>

                    </div>
                    <div class="input-div">
                        <label>Email</label>
                        <input type="text" name="email" value="{{$profile->email}}" required style="width: 100%;">


                    </div>


                    {{--                    <input type="text" name="phone" value="{{$profile->phone}}" required>--}}
                    <div class="input-div">
                        <label>Phone Number</label>
                    @if(isset($CountryCodes) && sizeof($CountryCodes))
                        <div class="phone-container">
                            <select name="phone_country_code"
                                    class="browser-default subject-field form-select control-25" id="phone_country_code"
                                    required>
                                @foreach($CountryCodes AS $CountryCode)
                                    <option value="{{ $CountryCode['code'] }}"
                                            @if( $CountryCode['phone_code']=='961') selected @endif>{{ $CountryCode['phone_code'] }}</option>
                                @endforeach
                            </select>

                            <input id="phone" type="text" placeholder="Phone Number" class="form-control control-75 "
                                   name="phone" value="{{old('phone')}}" required autocomplete="phone">
                        </div>
                    @endif

                    </div>

                    {{--                    @if(isset($Countries) && count($Countries)>0)--}}
                    {{--                        <select class="text-input" name="country" id="country" required>--}}
                    {{--                            @foreach($Countries as $country)--}}
                    {{--                                <option value="{{$country->code}}">{{$country->name_en}}</option>--}}

                    {{--                            @endforeach--}}
                    {{--                        </select>--}}
                    {{--                    @endif--}}


                    {{--                    ////////--}}

                    <div class="col-lg-6 mb-3 input-div">
                        <label>Country</label>

                        <div class="form-group" data-aos="fade-up" data-aos-delay="200">
                            {{--                            <label for="country" class="t-1-semibold d-block mb-2">Country</label>--}}
                            <select name="country" id="country" required class="@error('country') is-invalid @enderror">
                                <option value="" selected="" disabled="">Choose Your Country</option>

                                @foreach($Countries AS $Country)

                                    <option value="{{ $Country->code }}" data-state="{{$Country->state_required}}"
                                            data-zipcode="">{{ $Country->name }}</option>
                                @endforeach
                            </select>
                            @error('country')<span class="invalid-feedback"
                                                   role="alert"><strong>{{$message}}</strong></span>@enderror
                        </div>

                    </div>



                    <div class="col-lg-6 mb-3 input-div" id="state-container-wrapper">

                        <div class="form-group" id="state-container"></div>

                    </div>


                    <div class="col-lg-6 mb-3 input-div">

                        <div class="form-group address-city" id="city-container"></div>


                    </div>
                    {{--                    ////////--}}


{{--                    <div class="address-country-section" style="display: contents">--}}
{{--                        --}}{{--                        <input class="text-input half" type="text" name="city" id="" placeholder="City / State"--}}
{{--                        --}}{{--                               required>--}}

{{--                    </div>--}}
                    <div class="input-div">
                        <label>Street</label>
                        <input class="text-input half" type="text" name="street" id="" placeholder="street" required>
                    </div>
                    <div class="input-div">
                        <label>Building</label>
                        <input class="text-input" type="text" name="building" id="" placeholder="Building" required>
                    </div>

                    <div class="input-div">
                        <label>Floor</label>
                        <input class="text-input" type="text" name="floor" id="" placeholder="Floor" required>
                    </div>

                    <div class="input-div">
                        <label>Zip Code</label>
                        <input class="text-input half" type="text" name="zip_code" id="" placeholder="Zip Code" required>
                    </div>

                    <div>

                        <button class="address-btn" type="submit">Save</button>
                    </div>

                </div>
            </div>
        </form>

    </div>


</main>


<script language="javascript">

    function changeState() {

        $('.state-loader').removeClass('d-none');
        $.get('{{ ecom('url')->prefix() }}/account/addresses/states/list', {'country_code': $('#country').val()}, function (data) {
            $('#state-container').html(data);

        });
    }

    function changeCity() {

        $('.city-loader').removeClass('d-none');
        $.get('{{ ecom('url')->prefix() }}/account/addresses/cities/list', {
            'country_code': $('#country').val(), 'state': $('#state').val()
        }, function (data) {
            $('#city-container').html(data);
        });
    }

    function FooterFunctions() {

        $('#state').change(function () {

            $('.city-loader').removeClass('d-none');
            changeCity();
        });
        $('#country').change(function(){
            $('.state-loader').removeClass('d-none');
            changeState();
        });

        $('#country').change(function () {

            $('.state-loader').removeClass('d-none');

            if ($("#country option:selected").attr('data-state') == 0) {

                $('#state-container').addClass('d-none');

                changeCity();

            } else if ($("#country option:selected").attr('data-state') == 1) {

                $('#state-container').removeClass('d-none');

                changeState();
            }

            if ($("#country option:selected").attr('data-zipcode') == 0) {

                $('#zip-container').addClass('d-none');

            } else if ($("#country option:selected").attr('data-zipcode') == 1) {

                $('#zip-container').removeClass('d-none');
            }
        });

        changeState();
        changeCity();
    }
</script>
