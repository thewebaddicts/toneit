<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsPageModel extends Model
{
    protected $table= 'news';

    public function getImageAttribute()
    {
        return  env('DATA_URL') . "/news/" . $this->id . "." . $this->extension_image . "?v=." . $this->version;
    }
}
