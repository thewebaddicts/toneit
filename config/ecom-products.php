<?php

return [
    'data_url' => env('DATA_URL',env('BASE_DATA_URL', "enter the default value in config/ecom-products.php" )),
    'default_sorting' => 'orders' //can be price_as , price_desc, label_asc, label_desc
];
