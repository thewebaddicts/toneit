


@if(count($products['data'])>0)

<div class="product-container">
   
    @foreach ($products['data'] as $product)

{{--                @php $product = json_decode(json_encode($product)) @endphp--}}
        <div>
{{--        <div  style="background: url('/assets/svgs/eclipse-mobile.gif') 50% 50% no-repeat #0000;height: 100%;">--}}
            @component('pages.products.card',[ "product" => $product ]) @endcomponent
        </div>
    @endforeach


@component('components.pagination' , ['products'=>$products ]) @endcomponent


</div>
@else
       
       <div class="empty-customize">
              <div>
                     
                     <h3>
                            No product Found</h3></div>
       </div>

@endif