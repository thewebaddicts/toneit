<?php

namespace App\Http\Controllers;
use App\Models\FooterCarouselModel;
use App\Models\HelpandFaqModel;
use Illuminate\Http\Request;

class HelpandFaq extends Controller
{
    public static function render($store_prefix, $lang,$id)
    {

        $helpandfaq= HelpandFaqModel::where('cancelled', 0)->where('id',$id)->get();

        return view('pages.helpandfaq' , ['helpandfaq'=>$helpandfaq]);
    }
}
