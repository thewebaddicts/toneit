<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FooterCarouselModel extends Model
{
    protected $table='footer_carousel';
    use HasFactory;
}
