<?php $cartList = ecom('cart')->getAsArray();

try {
    $items = $cartList->items->items;
} catch (\Throwable $th) {
    $items = [];
}

?>
<?php //try { echo request()->user->counts['cart']; } catch (\Throwable $th) { echo "0"; } ?>
<form style="display: contents" method="post" action={{route('checkout-placeorder')}}>

    @csrf
    <input type="hidden" name="shipping_method" value="2">
    <input type="hidden" name="shipping_address" value="{{$cartList['info']['shipping_ecom_users_addresses_id']}}">
    <input type="hidden" name="billing_address" value="{{$cartList['info']['billing_ecom_users_addresses_id']}}">
    <input type="hidden" name="payment_method" value="{{$cartList['info']['ecom_stores_payment_methods_id']}}">
    <input type="hidden" name="delivery_date" value="null">
    <input type="hidden" name="delivery_date" value="null">

    <div class="checkout" style="width: 100%;">
        <picture>
            <div>
                <div class="parent">
                    <div style="visibility: hidden" class="prog-bar"></div>
                    <div style="background: #998484;color: white" class="square">1</div>
                    <div style="background: #998484;" class="prog-bar"></div>
                </div>

                <span>Shipping Address</span>

            </div>

            <div>
                <div class="parent">
                    <div style="background: #998484;" class="prog-bar"></div>
                    <div style="background: #998484;color: white" class="square">2
                    </div>
                    <div style="background: #998484;" class="prog-bar"></div>
                </div>

                <span>Payment Method</span>
                <div class="progress-bar"></div>
            </div>

            <div>
                <div class="parent">
                    <div style="background: #998484;" class="prog-bar"></div>
                    <div style="background: #998484;color: white" class="square">3</div>
                    <div style="visibility: hidden" class="prog-bar"></div>
                </div>

                <span>Order Summary</span>
                <div class="progress-bar"></div>
            </div>
        </picture>
        <section class="cart" style="width: 100%;">
            <div class="content">

                @if($cartList['items'])
                    @foreach($cartList['items'] as $item)
                    <main class="checkout-summary">


                        <picture><img loading="lazy" src="{{$item['details']['thumb'] }}' alt="></picture>
                        <div style=" padding-right: 100px;"><h3>{{$item['details']['label']}}</h3>
{{--                            <h4>Classic Black</h4>--}}
                        </div>


                        <span class="total">
                    <div class="quantity buttons_added">
                        {{--                        <button type="button" @if($item->details->stock_quantity < 1) disabled @endif class="minus" onclick="@isset($item->id) updateCartQuantity('{{$item->id}}' , -1)@endif"><i class="fas fa-minus"></i></button>--}}
                        <input type="number" @if($item['details']['stock_quantity'] < 1) disabled @endif step="1" min="1"
                               name="quantity" value="{{$item['quantity']}}" title="Qty"
                               class="input-text qty text" size="4" pattern="" inputmode="">
                        {{--                        <button type="button"@if($item->details->stock_quantity < 1) disabled @endif class="plus" onclick="@isset($item->id) updateCartQuantity('{{$item->id}}' , 1) @endif"><i class="fas fa-plus"></i></button>--}}

                    </div>
                               <span class="discount">{{$item['details']['unit_price_beforediscount_formatted']}}</span>
<div>
                            <span>{{$item['details']['unit_price_formatted']}}</span>
                    <span class="trash" data-target="remove-address-first"><img
                                onclick="removeItemFromCart({{$item['id']}})"
                                src="/assets/svgs/Icon feather-trash-2.svg" alt=""></span></div>
</span>
                    </main>
                @endforeach
                @endif
                    <div class="cart-breakdown">
           @component('components.cart-breakdown',['cartList'=>$cartList])@endcomponent</div>
                <div class="buttons">
                    <a>Previous</a>
                    <button type="submit">Checkout</button>


                </div>

            </div>
        </section>

    </div>
</form>