@if(Request::segment(3) == 'brands')



    <div class="sortby">

            <div class="info">
                @foreach ($brands as $key => $brand_array)<span><a style="display: contents" href="#{{$key}}">{{$key}}</a></span>        @endforeach



            </div>

        <div class="button">
            <form method="get" id='sort-by-id' style="display: contents">
                <div class="sortby-logo"><img src="/assets/svgs/Icon material-sort.svg" alt=""></div>
                <select class="nice-select" id="sort-by-name" name="sortBy" onchange="sortByChange($(this))" value="Sort By">

                    <option>Sort By</option>
                    <option @if(request()->sortBy === 'alphaA') selected @endif value="alphaA">
                        A-Z
                    </option>

                    <option @if(request()->sortBy === 'alphaD') selected @endif value="alphaD">
                        Z-A
                    </option>

                </select>
            </form>
        </div>
    </div>





@elseif(Request::segment(3) == 'news')
    <div class="sortby">
        <div class="info"></div>
        <div class="button">
            <form method="get" id='sort-by-id' style="display: contents">
                <div class="sortby-logo"><img src="/assets/svgs/Icon material-sort.svg" alt=""></div>
                <select class="nice-select" id="sort-by-name" name="sortBy" onchange="sortByChange($(this))" value="Sort By">

                    <option>Sort By</option>
                    <option @if(request()->sortBy === 'dateasc') selected @endif value="dateasc">
                        Date
                    </option>

                    <option @if(request()->sortBy === 'datedesc') selected @endif value="datedesc">
                        Date Old
                    </option>

                </select>
            </form>
        </div>
    </div>
@else
    <div class="sortby" @if(request()->routeIs('search')) style="display: none" @endif>
        <div class="info"></div>
        <div class="button">
            <form method="get" id='sort-by-id' style="display: contents">
                <div class="sortby-logo"><img src="/assets/svgs/Icon material-sort.svg" alt=""></div>
                <select class="nice-select" id="sort-by-name" name="sortBy" onchange="sortByChange($(this))" value="Sort By">

                    <option>Sort By</option>
                    {{--            <option @if(request()->sortBy === 'alpha') selected @endif value="alpha">--}}
                    {{--              A-Z--}}
                    {{--            </option>--}}

                    <option @if(request()->sortBy === 'priceA') selected @endif value="priceA">
                        Price Low
                    </option>
                    <option @if(request()->sortBy === 'priceD') selected @endif value="priceD">
                        Price High
                    </option>
                </select>
            </form>
        </div>
    </div>

@endif