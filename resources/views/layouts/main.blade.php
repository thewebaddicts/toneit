<?php $cartList = ecom('cart')->getAsObject();

?>
	<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta name="store-id" content="{{ request()->segment(1) }}">
	<meta name="lang" content="{{ request()->segment(2) }}">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta charset="utf-8">
	{{ seo()->setBladeInheritedTags(View::getSection('title'))->generate() }}
	<x-htmlhelpers-favicon prefix="/assets/favicon"></x-htmlhelpers-favicon>
	<meta name="description" content="We carry a variety of creams, dermo-cosmetics, multivitamins,
     serums, washes, oils, and more made in Italy, Poland, France, Sweden, among other countries.
      Brands we carry include Avene, Ducray, Advancis, Lactibiane, Boiron, Lakme, Filorga, Euronatural, Uriage, and more">
	<meta name="keywords"
	      content="Pharmacies in Lebanon, Online pharmacy store, Medicine store, Medicine online store, Pharmacies with delivery, Best pharmacies in Lebanon, Online pharmacy Lebanon, Skin care products, Best skin care products, Hair care, Healthy hair, Hair care products, Hair care routine, Professional hair care products, Cosmetics pharmacy, Pharmaceutical cosmetics, Supplements, Nutritional supplements, Recommended supplements, Supplements store, Vitamins and supplements stores, Pharmaceutical products, Online supplement store Lebanon">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap"
	      rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
	{{--    @yield('title')--}}
	<style>
           .common-loader-initial {
               position: fixed;
               top: 0;
               left: 0;
               right: 0;
               bottom: 0;
               width: 100vw;
               height: 100vh;
               margin: auto;
               background-color: #fff;
               z-index: 9999;
               background-image: url("/assets/svgs/loader.svg");
               background-position: center;
               background-repeat: no-repeat;
               background-size: 100px;
           }

           body,
           html {
               margin: 0;
               padding: 0;
               background-color: #FFF;
               max-width: 100%;
               overflow-x: hidden;
           }
	
	</style>
</head>

<body>

<div class="common-loader-initial common-loader-hide"></div>


<nav class="main">
	
	<div class="content">
		
		<a href="{{ route('home') }}" class="logo"><img src="/assets/svgs/Logo.svg" alt="logo"></a>
		
		<ul class="menu">
			
			{!!(ecom('menus')->get('mainMenu'))!!}
		
		</ul>
		
		<ul class="actions">
			<li class="search"><a href="#"><img src="/assets/svgs/search.svg"></a>
				<div class="togglesearch" style="">
					<form style="display: contents;" method="get" action="{{route('search')}}">
						@csrf
						<input type="text" placeholder="Search" name="term" id="search">
						<button type="submit">Search</button>
					</form>
				</div>
			</li>
			
			
			<li style="position: relative"><a href={{route('cart')}}>@if($cartList->count<1)<img
						src="/assets/svgs/cart.svg">@else <img
						src="/assets/svgs/Group 41.svg"> @endisset</a></li>
			
			{{--            <li><a href={{route('login')}}><img src="/assets/svgs/user.svg"></a></li>--}}
			@if(session('login'))
				<li><a href="{{route('account')}}"><img src="/assets/svgs/user.svg"></a></li>
			
			
			@else
				<li><a href="{{route('login',['customRouteName'=>'home'])}}"><img
							src="/assets/svgs/user.svg"></a></li>
				{{--                <div class="login"><a href="{{route('login',['customRouteName'=>'home'])}}">login</a></div>--}}
			@endif
		</ul>
	
	</div>
	
	{{--    <li class="searchbar">--}}
	{{--        <i class="fa fa-search" aria-hidden="true"></i>--}}
	{{--        <div class="togglesearch">--}}
	{{--            <input type="text" placeholder=""/>--}}
	{{--            <input type="button" value="Search"/>--}}
	{{--        </div>--}}
	{{--    </li>--}}
	<div class="content-responsive">
		<div class="menu-btn">
			<div class="menu-btn__burger">
			</div>
		</div>
		
		
		<div class="burger-display">
			<ul class="menu-responsive accordion" id="accordion">
				
				{!!(ecom('menus')->get('mainMenu'))!!}
			
			</ul>
		</div>
		
		
		<a style="width: 90px" href="{{ route('home') }}" class="logo"><img src="/assets/svgs/Logo.svg"
		                                                                    alt="logo"></a>
		<ul class="actions">
			<li class="search"><a href="#"><img src="/assets/svgs/search.svg"></a>
				<div class="togglesearch">
					<form style="display: contents;" method="get" action="{{route('search')}}">
						<input type="text" placeholder="Search" name="term">
						<button type="submit">Search</button>
					</form>
				</div>
			</li>
			<li><a href={{route('cart')}}>@if($cartList->count<1)<img
						src="/assets/svgs/cart.svg">@else <img
						src="/assets/svgs/Group 41.svg">@endisset</a></li>
			
			
			@if(session('login'))
				<li><a href="{{route('account')}}"><img src="/assets/svgs/user.svg"></a></li>
			
			
			@else
				<li><a href="{{route('login',['customRouteName'=>'home'])}}"><img
							src="/assets/svgs/user.svg"></a></li>
				{{--                <div class="login"><a href="{{route('login',['customRouteName'=>'home'])}}">login</a></div>--}}
			@endif
		</ul>
	
	
	</div>
</nav>

<a target="_blank" class="what" href="https://wa.me/96181106344?text=Hello%2C%20I%20have%20a%20question%20about%20your%20service.%20Can%20you%20please%20help%20me%3F">
	<img src="/assets/images/whatsapp.png" alt="">
</a>
@yield('content')

<sep></sep>
<sep></sep>


<sep></sep>
<sep></sep>
<sep></sep>
<sep></sep>

@include('layouts.footer')

<script type="text/javascript" src="/js/require.js"></script>
<script type="text/javascript" src="/js/init.js" attr-cache-version="{{ env('CACHE_VERSION') }}"></script>

{{--<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>--}}
{{--<script>--}}
{{--    window.OneSignal = window.OneSignal || [];--}}
{{--    OneSignal.push(function () {--}}
{{--        OneSignal.init({--}}
{{--            appId: "1be91a6d-c658-41d0-8356-0d3bdff487d7",--}}
{{--            safari_web_id: "web.onesignal.auto.05737452-6fb0-4a74-aaf2-b68bc5a2ed6a",--}}
{{--            notifyButton: {--}}
{{--                enable: true,--}}
{{--            },--}}
{{--        });--}}
{{--    });--}}
{{--</script>--}}
{{--{{ ecom('notifications')->showPopup() }}--}}

<script language="javascript">
    function NotificationFunction() {
	    @if (request()->input('notification'))
           ShowMessage("", "{{ request()->input('notification') }}");
        removeQueryNotificationString();
	    @endif
	    @if (request()->input('notification_title') && request()->input('notification_message'))
           ShowMessage("{{ request()->input('notification_title') }}", "{{ request()->input('notification_message') }}");
        removeQueryNotificationString();
	    @endif
	    @if (request()->input('notification_id'))
           <?php $notification = \App\Models\Notifications::getNotification(request()->input('notification_id'));
           ?>
           ShowMessage("{{ $notification->label }}", "{{ $notification->text }}");
        removeQueryNotificationString();
	    @endif
    }
</script>

<script language="javascript">
	{{--function changeState() {--}}
	
	{{--    $('.state-loader').removeClass('d-none');--}}
	{{--    $.get('{{ ecom('url')->prefix() }}/account/states/list', {'country_code': $('#country').val()}, function (data) {--}}
	{{--        $('#state-container').html(data);--}}
	{{--    });--}}
	{{--}--}}
	
	{{--function changeCity() {--}}
	
	{{--    $('.city-loader').removeClass('d-none');--}}
	{{--    $.get('{{ ecom('url')->prefix() }}/account/cities/list', {--}}
	{{--        'country_code': $('#country').val(),--}}
	{{--        'state': $('#state').val()--}}
	{{--    }, function (data) {--}}
	{{--        $('#city-container').html(data);--}}
	{{--    });--}}
	{{--}--}}
	
	{{--function FooterFunctions() {--}}
	
	{{--    $('#state').change(function () {--}}
	
	{{--        $('.city-loader').removeClass('d-none');--}}
	{{--        changeCity();--}}
	{{--    });--}}
	{{--    // $('#country').change(function(){--}}
	{{--    //     $('.state-loader').removeClass('d-none');--}}
	{{--    //     changeState();--}}
	{{--    // });--}}
	
	{{--    $('#country').change(function () {--}}
	
	{{--        $('.state-loader').removeClass('d-none');--}}
	
	{{--        if ($("#country option:selected").attr('data-state') == 0) {--}}
	
	{{--            $('#state-container').addClass('d-none');--}}
	
	{{--            changeCity();--}}
	
	{{--        } else if ($("#country option:selected").attr('data-state') == 1) {--}}
	
	{{--            $('#state-container').removeClass('d-none');--}}
	
	{{--            changeState();--}}
	{{--        }--}}
	
	{{--        if ($("#country option:selected").attr('data-zipcode') == 0) {--}}
	
	{{--            $('#zip-container').addClass('d-none');--}}
	
	{{--        } else if ($("#country option:selected").attr('data-zipcode') == 1) {--}}
	
	{{--            $('#zip-container').removeClass('d-none');--}}
	{{--        }--}}
	{{--    });--}}
	
	{{--    changeState();--}}
	{{--    changeCity();--}}
	{{--}--}}

</script>


</body>

</html>