@extends('layouts.main')
<title>Tonit Daily | {{ __('title.aboutus') }}</title>
@section('content')
    <section class="aboutus">

        <div style="background-image:url('{{env('DATA_URL')}}/aboutus/{{$aboutus[0]->id}}.{{$aboutus[0]->extension_image}}?v={{$aboutus[0]->version}}')">
            <div class="content">
                <div>
                    <span>Home/About Us</span>
                    <h1>About Us</h1>
                </div>
            </div>
        </div>

    </section>
    <section class="aboutus-gallery">


        <div class="content">

            <div>
                @isset($aboutus[0]->intro)
                <header>

                    <p>{!!nl2br($aboutus[0]->intro)!!}</p>

                </header>
                @endisset
                <main>
                    @php
                        $aboutus_json= json_decode($aboutus[0]['about_us']) @endphp


                        @foreach($aboutus_json as $about)

                        @isset($about->title)
                            <div @if($loop->index % 2 === 0) style="flex-direction: row-reverse"
                                 @endif class="first-row">

                                <picture>
                                    <img src={{env('DATA_URL')}}{{$about->image}} alt="">
                                </picture>

                                <div>

                                    <h3>{{$about->title}}</h3>
                                    <p>
                                        {!!nl2br($about->parag)!!}</p>
                                </div>


                            </div>
                        @endisset
                        @endforeach

                </main>
            </div>


        </div>
    </section>
@endsection
