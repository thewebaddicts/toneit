<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\Request;
use twa\htmlhelpers\models\SEOByRouteModel;
use twa\htmlhelpers\models\SEOSettingsModel;

class SEO
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    private $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }
    public function handle(Request $request, Closure $next)
    {
        $request->seo = false;
        $request->seo_settings = SEOSettingsModel::where('cancelled',0)->orderBy('orders')->first();
        $Routes = SEOByRouteModel::where('cancelled',0)->orderByRaw('CHAR_LENGTH(`route`) DESC')->get();

        foreach($Routes AS $route){
            if($request->is($route->route)){

                $seo = [];

                    $seo['en'] = [ "title" => $route["title"], "keywords" => $route["keywords"], "description" => $route["description"]  ];

                $request->seo = json_encode($seo);
                break;
            }
        }
        return $next($request);
    }
}
