@extends('layouts.main')
@section('content')
    @php

        $privacypolicy = getter('privacy_policy')->get();

    @endphp
    <div class="topbanner">
        <figure>
            <img src="/assets/images/Mask Group 30.png" alt="">
        </figure>
        <figcaption>
            <span>Home / Privacy Policy</span>
            <h1>Privacy Policy</h1>
            <h2> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</h2>

        </figcaption>
    </div>
    <section class="privacycookiesterms">
        <div class="content">
            @foreach($privacypolicy as $privacypolicies)
                <h3>{{$privacypolicies->title}}</h3>
                <p>{{$privacypolicies->text}}
                </p>
            @endforeach

        </div>
    </section>
@endsection
