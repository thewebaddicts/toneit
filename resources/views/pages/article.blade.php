@extends('layouts.main')
@section('content')
    <section class="article">
        <div class="articleback" style="background-image:url('{{$news['image']}}')">

            <div class="content">
                <div>
                    <span>{{$news['date']}}</span>
                    <h1>{{$news['title']}}</h1>
                </div>
                @php $socialmedia = getter('social_media')->condition('cancelled',0)->get(); @endphp

                <div class="socialmedia">
                    <ul>
                        <li><a target="_blank" href={{$socialmedia[0]->facebook_link}}><img src="/assets/svgs/Image 69.svg" alt=""></a></li>
                        <li><a target="_blank" href={{$socialmedia[0]->instagram_link}}><img src="/assets/svgs/Image 68.svg" alt=""></a></li>
                        <li><a target="_blank" href={{$socialmedia[0]->twitter_link}}><img src="/assets/svgs/Image 70.svg" alt=""></a></li>
                        <li><a target="_blank" href={{$socialmedia[0]->be_link}}><img src="/assets/svgs/Group 119.svg" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </div>

    </section>
    <section class="article-gallery">
        <div class="content">

            <div>

                <header>
                    <h2>Home / News / The Ordinary Moisturizing Package</h2>
                    <p>{{$news->intro_text}} </p>
                </header>
                @php

                    $newsjson = json_decode($news['paragraph_image'])
                @endphp

                <main>
                    @isset($newsjson)
                    @foreach($newsjson as $new)




                        <div @if($loop->index % 2 === 0) style="flex-direction: row-reverse" @endif class="first-row">
                            <div>
                                <h3>{{$new->title}}</h3>
                                <p>
                                    {{$new->paragraph}}</p>
                            </div>
                            <picture>
                                <img src={{env('DATA_URL')}}{{$new->image}} alt="">
                            </picture>


                        </div>

                    @endforeach
                        @endisset
                </main>
            </div>


        </div>
    </section>
@endsection
