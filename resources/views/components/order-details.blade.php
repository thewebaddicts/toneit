

<?php $purchases = ecom('purchases')->get(request()->segment(4)); ?>

@isset($purchases[0])

    <main id="orders-details">
        <h3>Orders Details</h3>
        <table style="width:100%">
            <tr>
                <th>Product</th>
                <th>Price</th>
            </tr>
         
                @php
                    $items = json_decode(json_encode($purchases[0]['items']))
                @endphp
                @foreach($items->items as $item)
                <tr>
                    <td>{{$item->details->label}}</td>
                    <td>{{$item->details->unit_price_formatted}}</td>
                </tr>
                @endforeach
        
            
        </table>
        <table style="width:100%">
            @foreach($items->breakdown as $breakdown)
            <tr>
       
                <th>{{$breakdown->label}}</th>
                <td>{{$breakdown->value_label}}</td>
            </tr>
            @endforeach
            <tr>
                <th>Payment Method</th>
                <td>@if($purchases[0]->payment_type == 'cod') Cash On Delivery @else Pay with Credit Card @endif</td>
            </tr>
            <tr>

                <th>Total</th>
                <td>{{$items->total->price}}</td>
            </tr>
        </table>
    </main>
@endisset
{{--<td>{{$purchase->id}}</td>--}}
{{--<td>{{$purchase->created_at}}</td>--}}
{{--<td>{{$purchase->status}}</td>--}}
{{--<td>{{$items->subtotal}}for {{$items->count}} </td>--}}