<?php
$item = ecom('products')->condition('ignoreListing',1)->condition('id',$item_id)->get();
$item = $item->products->data[0];
?>

<div class="item-added animation-toast">
    <span>Item successfully added to bag</span>
    <div class="item-flex">


        <picture><img id="expandedImg" src="{{$item['thumb'] }}">

        </picture>
        <div><span>{{ $item['label'] }}</span><span>{{$item['unit_price_formatted'] }}</span></div>

{{--<span>{{$cartList->count}}</span>--}}
    </div>
    <div class="item-btn">
        <a href="/">CONTINUE SHOPPING</a>
        <a href="{{ecom('url')->prefix()}}/cart">VIEW CART</a>
    </div>
</div>


