@extends('layouts.main')
@section('content')
@php

    $termsandconditions = getter('terms_conditions')->get();

@endphp
    <div class="topbanner">
        <figure>
            <img src="/assets/images/Mask Group 30.png" alt="">
        </figure>
        <figcaption>
            <span>Home / Terms & Conditions</span>
            <h1>Terms & Conditions</h1>
{{--            <h2> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</h2>--}}

        </figcaption>
    </div>
    <section class="privacycookiesterms">
        <div class="content">
@foreach($termsandconditions as $termsandcondition)
            <h3>{{$termsandcondition->title}}</h3>
            <p>{{$termsandcondition->text}}
            </p>
            @endforeach

        </div>
    </section>
@endsection
