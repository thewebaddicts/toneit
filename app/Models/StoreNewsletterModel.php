<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoreNewsletterModel extends Model
{
    protected $table = 'store_newsletter';
    use HasFactory;
}
