@extends('layouts.main')
@section('content')
    @php
        $profile = ecom('profile')->get();
        $addresses = ecom('addresses')->list();
    @endphp

    <div class="topbanner">
        <figure>
            <img src="/assets/images/Mask Group 30.png" alt="">
        </figure>
        <figcaption>
            <span>Home / Account</span>
            <h1>Account</h1>


        </figcaption>
    </div>
    <section class="account-info">
        <div class="content">
            <div>
                @component('components.account-sidebar')@endcomponent



                @component('components.order-details')@endcomponent





            </div>

        </div>


    </section>
@endsection