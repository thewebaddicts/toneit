<?php

namespace App\Http\Controllers;
use App\Models\FooterCarouselModel;
use Illuminate\Http\Request;
use App\Models\PageProductModel;
use App\Models\RecommendedProductModel;
class ProductController extends Controller
{
    public static function render($id , $label)
    {
    
        $recommendedproduct = RecommendedProductModel::where('cancelled', 0)->get();
        $productspage = PageProductModel::where('cancelled', 0)->where('id',$id)->first();
        try {
            $items = json_decode($productspage->product_gallery);
          
        } catch (\Throwable $th) {
            $items = [];
            
        }
    

        return view('pages.product' , ['productspage'=>$productspage,'recommendedproduct'=>$recommendedproduct , 'items'=>$items]);
        
    }
}
