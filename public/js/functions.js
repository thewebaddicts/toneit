// function wtspIcon() {
//     var config = {
//         phone: "96181106344",
//         call: "Message us",
//         position: "ww-right",
//         size: "ww-normal",
//         text: "Hi, how can I help you with?",
//         type: "ww-extended",
//         brand: "TonitDaily",
//         subtitle: "",
//         welcome: ""
//     };
//     var proto = document.location.protocol, host = "cloudfront.net", url = proto + "//d3kzab8jj16n2f." + host;
//     var s = document.createElement("script");
//     s.type = "text/javascript";
//     s.async = true;
//     s.src = url + "/v2/main.js";
//     s.onload = function () {
//         tmWidgetInit(config)
//     };
//     var x = document.getElementsByTagName("script")[0];
//     x.parentNode.insertBefore(s, x);
//
// }

function removeDisplay() {
    $('.level-2 li').mouseover(function () {

        $('.level-2 li').find('ul').removeClass('Display');
        $(this).find('ul').addClass('Display');
    })
}

function accordion() {
    var Accordion = function (el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        // Variables privadas
        var links = this.el.find('li a');
        // Evento
        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }

    Accordion.prototype.dropdown = function (e) {
        var $el = e.data.el;
        $this = $(this),
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
        }

    }

    var accordion = new Accordion($('#accordion'), false);
}

function removeQueryNotificationString() {

    urlObject = new URL(window.location.href);
    urlObject.searchParams.delete('notification');
    urlObject.searchParams.delete('notification_message');
    urlObject.searchParams.delete('notification_title');

    url = urlObject.href;

    history.pushState(null, null, url);

}

function filterCustom() {
    $('.sublist .filter-custom label').click(function () {
        $(this).find('div').toggleClass('active');
    })
}

function wisthlistHeart() {
    $('.product-details .info header #ko').on("click", function (e) {
        alert()
        $('#myImg').toggle('slow');
    });
}

function disableButton() {

    $('form #submit').prop("disabled", true);
    $('.sub-box').click(function () {
        if ($(this).find('#radio').is(':checked')) {
            $('.sub-box').prop('disabled', false);

        } else {

            if ($('.checks').filter(':checked').length < 1) {

                $('#submit').prop("disabled", false);
            }

        }
    });
}

function disableButton2() {

    $('form #submit').prop("disabled", true);
    $('.container input').click(function () {
        if ($(this).find('#radio').is(':checked')) {
            $('.sub-box').prop('disabled', false);

        } else {

            if ($('.checks').filter(':checked').length < 1) {

                $('#submit').prop("disabled", false);
            }

        }
    });
}

function specialButton() {
    $('.sub-box').click(function () {
        $(this).find('header input[type=radio]').prop('checked', true);
        $('.sub-box header span .checkmark').removeClass('select');
        $(this).find('header span .checkmark').addClass('select');
    });
}

function ToggleFavoritesInit() {

    $('.favorite-toggle').click(function (e) {
        $(this).find('.fa-heart').toggleClass('fas');
        e.preventDefault();

        var elem = $(this);
        var productID = elem.attr('data-pid');
        var url = elem.hasClass('favorite-toggle-active') ? "favorites/remove" : "favorites/add";

        var token = $("meta[name=csrf-token]").attr("content");
        var store_id = $("meta[name=store-id]").attr("content");
        var lang = $("meta[name=lang]").attr("content");

        $.ajax({
            type: "POST",
            url: "/" + store_id + "/" + lang + "/" + url,
            data: {
                productID: productID,
                store: store_id,
                lang: lang,
                _token: token
            },
            success: function (data) {
                elem.toggleClass('favorite-toggle-active');

            }
        });
    });


    $('.favorite-remove').click(function () {
        var elem = $(this);

        var item = elem.parents('.wishlist-item');

        var productID = elem.attr('data-pid');
        var url = "favorites/remove";

        var token = $("meta[name=csrf-token]").attr("content");
        var store_id = $("meta[name=store-id]").attr("content");
        var lang = $("meta[name=lang]").attr("content");

        $.fancyConfirm({
            title: "Are you sure?",
            message: "Are you sure you want to remove this item from wishlist?",
            okButton: "Agree",
            noButton: "Disagree",
            callback: function (value) {
                if (value) {
                    $.ajax({
                        type: "POST",
                        url: "/" + store_id + "/" + lang + "/" + url,
                        data: {
                            productID: productID,
                            store: store_id,
                            lang: lang,
                            _token: token
                        },
                        success: function (data) {
                            item.remove();
                        }
                    });
                }
            },
        });

    });


}


function sortByChange() {
    $('#sort-by-id').submit();
}

function iconFormSubmit() {
    $(document).ready(function () {
        $('.trash').click(function () {

            $(this).parents('.icon-form').submit();

        })

    });
}


function removeItemFromCart(cartItemID) {
    var local = $("meta[name=csrf-token]").attr("content");
    var store_id = $("meta[name=store-id]").attr("content");
    var lang = $("meta[name=lang]").attr("content");

    $.fancyConfirm({
        title: "Are you sure?",
        message: "Are you sure you want to remove this item from cart?",
        okButton: "Agree",
        noButton: "Disagree",
        callback: function (value) {
            if (value) {
                $("#cart-info").addClass("loading-box");
                $("#cart-details").addClass("loading-box");

                $.ajax({
                    type: "POST",
                    url: "/" + store_id + "/" + lang + "/cart/remove",
                    data: {
                        cartItemID: cartItemID,
                        store: store_id,
                        lang: lang,
                        _token: local,
                    },

                    success: function (data) {

                        if (data.status || data.status == "true") {
                            location.reload();
                            // $("#cart-details").html(data.breakdown);
                            // $('.cart-count').text(data.total);
                            //
                            // $("#cart-info").removeClass("loading-box");
                            // $("#cart-details").removeClass("loading-box");

                        } else {
                        }
                    },
                });
            }
        },
    });
}

function updateCartQuantity(cartItemID, direction) {

    var local = $("meta[name=csrf-token]").attr("content");
    var store_id = $("meta[name=store-id]").attr("content");
    var lang = $("meta[name=lang]").attr("content");

    $.ajax({
        type: "POST",
        url: "/" + store_id + "/" + lang + "/cart/update-quantity",
        data: {
            direction: direction,
            cartItemID: cartItemID,
            store: store_id,
            lang: lang,
            _token: local
        },

        success: function (data) {

            $('.cart-count').html(data.total);
            location.reload();
        }
    });
}

function redirectWithCheckedAddress(url, arr) {
    var items = [];
    arr.forEach((item, index) => {
        items.push({
            key: item,
            value:
                $("input[name=" + item + "]:checked").length > 0
                    ? $("input[name=" + item + "]:checked").val()
                    : $("#" + item).val()
        });
    });
    var str = "";
    var obj = getParams();
    if (!jQuery.isEmptyObject(obj)) {
        var added = true;
        Object.keys(obj).forEach((key, index) => {
            if (!arr.includes(key)) {
                str +=
                    index == 0
                        ? "?" + key + "=" + obj[key]
                        : "&" + key + "=" + obj[key];
                added = added && false;
            }
        });
        items.forEach((item, index) => {
            if (index == 0) {
                str += added
                    ? "?" + item.key + "=" + item.value
                    : "&" + item.key + "=" + item.value;
            } else {
                str += "&" + item.key + "=" + item.value;
            }
        });
    } else {
        items.forEach((item, index) => {
            if (index == 0) {
                str += "?" + item.key + "=" + item.value;
            } else {
                str += "&" + item.key + "=" + item.value;
            }
        });
    }
    return (window.location.href = url + str);
}

function getParams() {
    var url = window.location.href;
    if (url.indexOf("?") == -1) {
        return {};
    }
    var params = {};
    var parser = document.createElement("a");
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
}

function toggleWishlist(event, elem, item_id, csrf, type) {
    event.preventDefault();
    let rqst_url = "";
    if (type == "add") {
        rqst_url = "/wishlist/add";
    } else if (type == "remove") {
        rqst_url = "/wishlist/remove";
    }

    $.ajax({
        type: "POST",
        url: rqst_url,
        data: {_token: csrf, item_id: item_id},
        success: function (data) {
            if (type == "add") {
                elem.parent()
                    .find(".favorite-add")
                    .removeClass("d-block")
                    .addClass("d-none");
                elem.parent()
                    .find(".favorite-remove")
                    .removeClass("d-none")
                    .addClass("d-block");
            } else {
                elem.parent()
                    .find(".favorite-remove")
                    .removeClass("d-block")
                    .addClass("d-none");
                elem.parent()
                    .find(".favorite-add")
                    .removeClass("d-none")
                    .addClass("d-block");
            }
        },
        error: function (e) {
            console.log(e)
        }
    });
}

function removeFromWishlist(event, item_id, csrf) {
    event.preventDefault();
    $.fancyConfirm({
        title: "Are you sure?",
        message: "Are you sure you want to remove this item from your wishlist?",
        okButton: "Agree",
        noButton: "Disagree",
        callback: function (value) {
            if (value) {
                $.ajax({
                    type: "POST",
                    url: "/wishlist/remove",
                    data: {
                        _token: csrf,
                        item_id: item_id,
                        return_view: true
                    },
                    success: function (data) {
                        console.log(data.view);
                        $("#wishlist-body").html(data.view);
                    }
                });
            } else {
            }
        }
    });
}


function mouseDown_i($val) {
    Title = 'You Exceeded the stock quantity of this product';

    value = isNaN(parseInt(document.getElementById('xxxx').value)) ? 0 : parseInt(document.getElementById('xxxx').value);
    if (value < $val) {
        document.getElementById('xxxx').value = value + 1;
        timeout_ = setTimeout(function () {
            mouseDown_i();
        }, 150);

    } else {
        $.fancybox.open(
            '<div class="message"><h2>' + Title + "</h2></div>"
        );
    }

}

function checkPass() {
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    var message = document.getElementById('error-nwl');
    var goodColor = "#fff";
    var badColor = "#998484";

    if (pass1.value.length > 5 && pass2.value.length > 5) {
        pass1.style.backgroundColor = "#fff";
        pass2.style.backgroundColor = "white";
        message.style.color = goodColor;
        message.innerHTML = "character number ok!"

        if (pass1.value == pass2.value) {
            pass2.style.backgroundColor = "#9ED6E5s";
            message.style.color = goodColor;
            message.innerHTML = "ok!"
        } else {
            pass2.style.backgroundColor = "#998484";
            message.style.color = badColor;
            message.innerHTML = " These passwords don't match"
        }
    } else {
        pass1.style.backgroundColor = "white";
        pass2.style.backgroundColor = "white";
        message.style.color = badColor;
        message.innerHTML = " you have to enter at least 6 digit!"
    }

    if (pass1.value != pass2.value) {
        pass2.setCustomValidity("Passwords Don't Match");
    } else {
        pass2.setCustomValidity('');
    }
}

function mouseDown_d() {
    value = isNaN(parseInt(document.getElementById('xxxx').value)) ? 0 : parseInt(document.getElementById('xxxx').value);
    value - 1 <= 0 ? document.getElementById('xxxx').value = '1' : document.getElementById('xxxx').value = value - 1;
    timeout_ = setTimeout(function () {
        mouseDown_d();
    }, 150);
}

function mouseUp() {
    clearTimeout(timeout_);
}

function mouseLeave() {
    clearTimeout(timeout_);
}


function ShowLoader() {
    $('body').append('<div class="common-loader-initial common-loader-noanim"></div>');
}

function HideLoader() {
    $('.common-loader-initial').remove();
}

function ShowMessage(Title, Text) {
    $.fancybox.open(
        '<div class="message"><h2>' + Title + "</h2><p>" + Text + "</p></div>"
    );
}

function ShowIframe(link, maxwidth) {
    $.fancybox.open({
        src: link,
        type: "iframe",
        opts: {
            iframe: {
                css: {
                    width: maxwidth
                }
            }
        }
    });
}

function closeFancyBox() {
    parent.jQuery.fancybox.getInstance().close();
}

var isMobile = false; //initiate as false
// device detection
if (
    /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
        navigator.userAgent
    ) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
        navigator.userAgent.substr(0, 4)
    )
) {
    isMobile = true;
}
// function search() {
//     $(document).ready(function () {
//
//         $(".search a img").click(function () {
//             $(".togglesearch").toggleClass('active');
//             $("#search #inp").focus();
//         });
//         $('.fa-xmark').click(function () {
//             $(this).parent('.togglesearch').removeClass('active');
//
//         });
//
//     });
//
// }

function closeSearch() {
    $('body').on('click', function (event) {

        if ($(event.target).parents('.search').find('.togglesearch').hasClass("active")) {
            $('.togglesearch').addClass('active');
        } else {
            $('.togglesearch').removeClass('active');
        }
    })
}

function search() {
    $(".search a").click(function () {
        $(this).parent(".search").find(".togglesearch").addClass('active');
        $("#search").focus();
    });
}

// function closeSearch() {
//
//     $('body').on('click', function (event) {
//
//         if ($(event.target).parent('.content').find('.togglesearch').hasClass("active")) {
//             $('.togglesearch').addClass('active');
//         } else {
//             $('.togglesearch').removeClass('active');
//         }
//     })
// }


function applyVoucher(elem) {

    var input = elem.parent().find("input[name=promo_code]");
    var code = input.val();

    var token = $("meta[name=csrf-token]").attr("content");
    var store_id = $("meta[name=store-id]").attr("content");
    var lang = $("meta[name=lang]").attr("content");

    elem.prop("disabled", true);
    elem.addClass("spin");

    if (elem.attr("data-type") == "remove") {
        alert(elem.attr("data-type"))
        var url = "/" + store_id + "/" + lang + "/voucher/remove";
        var opp_type = "apply";
        var input_disabled = false;
    } else {
        alert(elem.attr("data-type"))
        var url = "/" + store_id + "/" + lang + "/voucher/apply";
        var opp_type = "remove";
        var input_disabled = true;
    }

    var params = getParams();

    $.ajax({
        type: "POST",
        url: url,
        data: {
            code: code,
            store: store_id,
            lang: lang,
            _token: token,
            ...params,
        },
        success: function (data) {
            console.log(data);
            if (data.status || data.status == "true") {
                elem.attr("data-type", opp_type);

                elem.find(".btn-text").text(
                    elem.attr("data-" + opp_type + "-text")
                );

                if (!input_disabled) {
                    input.val("");
                }

                input.prop("disabled", input_disabled);

            } else {
                alert(data.message);
            }


            $(".cart-breakdown").html(data.breakdown);


            elem.prop("disabled", false);
            elem.removeClass("spin");
        },
    });
}

function addToCart(form, event = false) {
    if (event) {
        event.stopPropagation();
        event.preventDefault();
    }
    var store_id = $("meta[name=store-id]").attr("content");
    var lang = $("meta[name=lang]").attr("content");


    var local = $("meta[name=locale]").attr("content");


    $.ajax({
        type: "POST",
        url: "/" + store_id + "/" + lang + "/cart/add",
        data: form,
        success: function (data) {
            console.log(data);
            if (data.success || data.success == "true") {
                // $.fancybox.open(data.view);
                $("body").append(data.view);
                $('.cart-count').text(data.count);
                setTimeout(function () {
                    $("body").find(".item-added").remove();
                }, 1000000);
            } else {
            }
        },
    });
}

function filter() {
    $(".filter-sublist li input").change(function () {


        var inputs = $('#filter-products-form').serializeArray();
        var url = window.location.pathname;

        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });


        $.ajax({
            type: "POST",
            url: url,
            data: inputs,
            success: function (data) {
                $('#product-listing-container').html(data);
            },
            error: function (err) {
                console.log(err);
            },
        });

        // window.history.pushState('', 'New Page Title', 'http://toneit.test/lb/en/products/list/10/make_up?filter=');

    });
}

function filterBrands() {
    $(".brand-sublist li input").change(function () {


        var inputs = $('#filter-brands-form').serializeArray();
        var url = window.location.pathname;

        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        $.ajax({
            type: "POST",
            url: url,
            data: inputs,
            success: function (data) {
                $('#product-listing-container').html(data);
            },
            error: function (err) {
                console.log(err);
            },
        });

        // window.history.pushState('', 'New Page Title', 'http://toneit.test/lb/en/products/list/10/make_up?filter=');

    });
}

function insertUrlParam(key, value) {
    if (history.pushState) {
        let searchParams = new URLSearchParams(window.location.search);
        searchParams.set(key, value);
        let newurl =
            window.location.protocol +
            "//" +
            window.location.host +
            window.location.pathname +
            "?" +
            searchParams.toString();
        window.history.pushState({path: newurl}, "", newurl);
    }
}


function myFunction(imgs) {
    var expandImg = document.getElementById("expandedImg");

    expandImg.src = imgs.src;

    expandImg.parentElement.style.display = "block";
}

// function doItem() {
//     $('.menu-responsive').find('.level-m').css('display', 'none');
//
// }

// function menuResponsive() {
//     $('.menu-responsive .level-2 li a').first().attr("href", "javascript:;");
//     $('.menu-responsive ul li').click(function (e) {
//
//         // $('.menu-responsive .level-1 li .level-2').removeClass('expand');
//         $(this).find('ul').first().toggleClass('expand');
//         e.stopPropagation();
//     });
// }

function hover() {

    $(document).ready(function () {

        $(".level-1 li").click(function () {
            // $(this).find('.level-2').slideDown();

            $(".level-1 li .level-2").removeClass('Display');

            $(this).find('.level-2').first().addClass('Display');

        });

    });
}


// function doSubmenu() {
//
//     $('.level-2 li a').first().toggleClass('Display');
//     $('.level-2 li ul').first().toggleClass('Display');
//
// }


function editFeature() {

    $('#second form .box .sub-box div .edit-feature').on("click", function () {

        $('.account-info .content div #second').removeClass('active');
        $('#' + $(this).data('target')).addClass('active');
    });

}

function removeFeature() {

    $('#second form .box .sub-box div .trash').on("click", function () {
        $('#' + $(this).data('target')).css('visibility', 'hidden');
    });

}


function addAddress() {

    $('#second header button').on("click", function () {
        if ($('#remove-address-first').css('visibility', 'visible')) {
            $('#remove-address-second').css('visibility', 'visible');
        } else {
            $('#remove-address-first').css('visibility', 'visible');
        }
    });

}

function doSub() {

    $('.account-info .content div ul li').on("click", function () {

        $('.account-info .content div main').removeClass('active');
        $('#' + $(this).data('target')).addClass('active');
    });

}

function doOrders() {

    $('#orders table tr td button').on("click", function () {

        $('.account-info .content div main').removeClass('active');
        $('#' + $(this).data('target')).addClass('active');
    });

}

function clickChange() {
    $('div picture').on("click", function () {

        $('.images div picture').removeClass('active');
        $(this).addClass('active');
    })
}

function doAccordion() {
    $('.accordion .expandable').slideUp(0);
    $('.accordion a').on('click', function () {
        $(this).parents('.accordion').find('span img').toggleClass('expanded');
        $(this).parents('.accordion').find('.expandable').slideToggle(300);

    });

}


function burger() {
    const menuBtn = document.querySelector('.menu-btn');
    const menuDiv = document.querySelector('.burger-display');
    let menuOpen = false;
    menuBtn.addEventListener('click', () => {

        if (!menuOpen) {
            menuBtn.classList.add('open');
            menuDiv.classList.add('open');
            menuOpen = true;


        } else {
            menuBtn.classList.remove('open');
            menuDiv.classList.remove('open');
            menuOpen = false;
        }

    })
}


function doParallax() {
    $(".parallax, .parallaxText").css({
        transition: "0s !important",
        "transition-delay": "0s !important",
        "transition-timing-function": "linear"
    });
    if (isMobile != true) {
        $(".parallax").each(function () {
            var offset = $(this).offset();
            var positionY = (window.pageYOffset - offset.top) / 2;
            if (positionY < 0) {
                positionY = 0;
            }
            if (positionY < 100) {
                $(this)
                    .find(".parallaxText")
                    .css("opacity", "" + (100 - positionY) / 100);
            }
            $(this).css("background-position", "center +" + positionY + "px");
        });
    }
}

function InitializeMenuScroll() {
    var scroll_start = 0;
    var startchange = $(".menu-marker");
    var offset = startchange.offset();
    //  var slideshowOffset = $("#slideshow").offset();
    if (startchange.length >= 0 && $(document).scrollTop() - offset.top >= 0) {
        $("nav.main").addClass("compact");
        $("ul.main-menu.level-2").css("top", "70px");
        $(".togglesearch").addClass("compact");
    } else {
        $("nav.main").removeClass("compact");
        $("ul.main-menu.level-2").css("top", "100px");
        $(".togglesearch").removeClass("compact");
    }
}

function MenuScroll() {
    var scroll_start = 0;
    var startchange = $(".menu-marker");
    var offset = startchange.offset();
    //  var slideshowOffset = $("#slideshow").offset();
    if (startchange.length >= 0 && $(document).scrollTop() - offset.top >= 0) {
        $("div.drop-down-menu").addClass("compact");
    } else {
        $("div.drop-down-menu").removeClass("compact");
    }
}

// function custom(){
//     $('.main-menu.level-4').append('<a id="innerLink">View More</a>');
// }

