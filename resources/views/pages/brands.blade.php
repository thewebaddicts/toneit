@extends('layouts.main')

@section('content')



    <div class="topbanner">
        <figure>
            <img src="/assets/images/Mask Group 30.png" alt="">
        </figure>
        <figcaption>
            <span>Home / Brands</span>
            <h1>Brands</h1>
            <h2> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</h2>

        </figcaption>
    </div>
    <section class="brands news">
        <div class="content">

            @component('components.sortby',['brands'=>$brands])@endcomponent

            @foreach ($brands as $key => $brand_array)
                <main>
                    <h1 id="{{$key}}">{{ $key }}</h1>
                    <div>
                        <ul>

                            @foreach ($brand_array as $brand)


                                <li><a href="{{ ecom('url')->prefix() }}/products/brand/{{$brand->id }}/{{Str::slug($brand->label) }}">{{$brand->label}}</a></li>

                            @endforeach
                        </ul>


                    </div>
                </main>

            @endforeach


        </div>
    </section>
    <section class="brand-logo">

        <div>
            <div class="content">
                <h3>Most Popular Brands</h3>
                <div class="brandslogo">
                    <div><img src="/assets/images/loreal.png" alt=""></div>
                    <div><img src="/assets/images/maybeline.png" alt=""></div>
                    <div><img src="/assets/images/lancome.png" alt=""></div>
                    <div><img src="/assets/images/guerlain.png" alt=""></div>
                    <div><img src="/assets/images/esteelauder.png" alt=""></div>
                    <div><img src="/assets/images/dior.png" alt=""></div>
                </div>


            </div>


        </div>


    </section>
@endsection
