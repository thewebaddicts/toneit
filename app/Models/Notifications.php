<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table = "notifications";
    public static function getNotification($id){
        return self::where('id',$id)->first();
    }
    use HasFactory;
}
