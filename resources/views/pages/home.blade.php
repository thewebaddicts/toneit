@extends('layouts.main')
{{--<title>Tonit Daily | {{ __('title.home') }}</title>--}}

@section('content')
    <?php
    //    $collections = ecom('collections')->condition('display_homepage', 1)->condition('IncludeProducts', 1)->get();
    $collections = ecom('collections')->condition('display', 1)->condition('cancelled', 0)->condition('collectionID', 1)->condition('ignoreListing', 1)->condition('display', 1)->condition('IncludeProducts', 1)->get();
    $shopbycategory = ecom('sections')->condition('IncludeCollections', 1)->condition('display', 1)->condition('IncludeProducts', 1)->condition('cancelled', 0)->get();

    ?>

    <div class="slideshow carousel owl-carousel" data-carousel-items="1" data-carousel-dots="false"
         data-carousel-loop="true" data-carousel-nav="true" data-carousel-autoplay="true"
         data-carousel-autowidth="false">

        @foreach ($slideshows as $slideshow)

            <div class="slide"
                 style="background-image: url('{{ env('DATA_URL') }}/slideshow/{{ $slideshow->id }}.{{ $slideshow->extension_image }}?v={{ $slideshow->version }}')">
                <div class="content">
                    <div class="slide-title">
                        <div class="progress"></div>
                        @isset($slideshow->label)
                            <h1>{{ $slideshow->label }}</h1>
                        @endisset
                        @isset($slideshow->text)

                            <h2>{!!nl2br($slideshow->text)!!}</h2>
                        @endisset
                        <sep></sep>
                        <sep></sep>
                        <sep></sep>
                        <a href="{{$slideshow->btn_link}}">View More</a>
                    </div>

                </div>
            </div>
        @endforeach

    </div>

    <div class="slideshow-mobile carousel owl-carousel" data-carousel-items="1" data-carousel-dots="false"
         data-carousel-loop="true" data-carousel-nav="true" data-carousel-autoplay="true"
         data-carousel-autowidth="false">

        @foreach ($slideshows as $slideshow)

            <div class="slide"
                 style="background-image: url('{{ env('DATA_URL') }}/slideshow_mobile/{{ $slideshow->id }}.{{ $slideshow->extension_image }}?v={{ $slideshow->version }}')">
                <div class="content">
                    <div class="slide-title">
                        <div class="progress"></div>
                        @isset($slideshow->label)
                            <h1>{{ $slideshow->label }}</h1>
                        @endisset
                        @isset($slideshow->text)

                            <h2>{!!nl2br($slideshow->text)!!}</h2>
                        @endisset
                        <sep></sep>
                        <sep></sep>
                        <sep></sep>
                        <a href="{{$slideshow->btn_link}}">View More</a>
                    </div>

                </div>
            </div>
        @endforeach

    </div>


    <div class="menu-marker"></div>
    <section class="second-container">
        <div class="slide" style="background-image: url('/assets/images/backgroud.png')">

            <div class="content">
                {{--                            @dd($collections)--}}
                @if(isset($collections[5]))
                    @component('components.products.collection',['collection'=>$collections[5]])@endcomponent
                @endif
            </div>
        </div>
    </section>
    {{--    <section class="second-container">--}}
    {{--        <div class="slide" style="background-image: url('/assets/images/backgroud.png')">--}}

    {{--            <div class="content">--}}

    {{--                @isset($mainproduct)--}}
    {{--                    <div class="slide-title">--}}

    {{--                        <h1>{{ $mainproduct->title }}</h1>--}}
    {{--                        <h2>{{$mainproduct->sub_title }}</h2>--}}
    {{--                        <p>{!!nl2br($mainproduct->paragraph)!!}</p>--}}
    {{--                        <sep></sep>--}}
    {{--                        @if($mainproduct->btn_link > 0 && $mainproduct->btn_label > 0)--}}
    {{--                            <a href="{{$mainproduct->btn_link}}">{{ $mainproduct->btn_label}}</a>--}}
    {{--                        @endif--}}
    {{--                    </div>--}}
    {{--                    <div class="slide-image">--}}
    {{--                        <div>--}}
    {{--                            <img--}}
    {{--                                    src="{{ env('DATA_URL') }}/mainproduct/{{ $mainproduct->id }}.{{ $mainproduct->extension_image}}?v={{ $mainproduct->version }}' alt=">--}}
    {{--                        </div>--}}

    {{--                    </div>--}}
    {{--                @endisset--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}









    <section class="third-container">
        <div class="content">
            <h1>{{$shopbycategory[0]->label}}</h1>

            <div class="carousel multi-carousel" data-carousel-dots="false" data-carousel-items="4"
                 data-carousel-loop="true" data-carousel-nav="false" data-carousel-autoplay="true"
                 data-carousel-autowidth="true">

                @foreach ($shopbycategory[0]->collections as $shopbycategories)

                    <a href="{{ecom('url')->prefix() }}/products/subcategory/{{ Str::slug($shopbycategories->slug) }}">
                        <figure>


                            <img
                                    src="{{ env('DATA_URL') }}/ecom_collections/{{ $shopbycategories->id }}.{{ $shopbycategories->extension_image }}?v={{ $shopbycategories->version }}' alt=">
                            <figcaption>{{ $shopbycategories->label }}</figcaption>
                        </figure>
                    </a>
                @endforeach

            </div>

        </div>
    </section>
    <?php
    $collections = ecom('collections')->condition('display_homepage', 1)->condition('display', 1)->condition('IncludeProducts', 1)->get();

    ?>


    <section class="fourth-container">
        <div class="content">
            @foreach($collections as $collection)
                <div class="container" style="padding-left: 0px">
                    <h1>{{$collection->label}}</h1>

                    <h2>{{$collection->description}} </h2>
                    <div class="product-container">


                        @foreach($collection->products->data as $products)
                            {{--                            @php $product = json_decode(json_encode($products)) @endphp--}}

                            @if($loop->index<=3)
                                <div>

                                    @component('pages.products.card',[ "product" => $products ]) @endcomponent
                                </div>
                            @endif
                        @endforeach



                        {{--                @foreach ($bestsells as $bestsell)--}}
                        {{--                    <div><img--}}
                        {{--                            src="{{ env('DATA_URL') }}/best_sells/{{ $bestsell->id }}.{{ $bestsell->extension_image }}?v={{ $bestsell->version }}"--}}
                        {{--                            alt="">--}}
                        {{--                        <a href="">{{ $bestsell->btn_label }}</a>--}}
                        {{--                        <h3>{{ $bestsell->brand_name }}</h3>--}}
                        {{--                        <span>{{ $bestsell->product_short_description }}</span><span class="product-price">{{ $bestsell->product_price }}L.L</span>--}}
                        {{--                    </div>--}}
                        {{--                @endforeach--}}

                    </div>

                </div>
            @endforeach
        </div>
    </section>


    <section class="fifth-container">
        <div class="content">
            @foreach($news as $new)
                @component('partials.product-article',[ "new" => $new ]) @endcomponent
            @endforeach
        </div>
    </section>
@endsection