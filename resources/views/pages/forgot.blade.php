@extends('layouts.main')

@section('content')
    <div class="topbanner">
        <figure>
            <img src="/assets/images/Mask Group 30.png" alt="">
        </figure>
        <figcaption>
            <span>Home / Account</span>
            <h1>Account</h1>


        </figcaption>
    </div>

    <section class="forget-password">

        <div class="content">
            <header>
                <h2>Forget Password</h2>
            </header>
            <form style="display: contents;" method="post" action={{route('forgot-password')}}>
                @csrf
            <main>

                <div>
                    <label for="">Email</label>
                    <input type="email" placeholder="Your Email ..." name="email">
                </div>

                <input type="submit">
            </main>

            </form>
        </div>


    </section>

@endsection