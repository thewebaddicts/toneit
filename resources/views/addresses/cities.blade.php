<?php
use Illuminate\Support\Facades\Http;

?>

<div class="form-group">
    <i class="fas fa-spinner fa-spin city-loader d-none"></i>

    <label class="t-1-semibold d-block mb-2">{{__('Cities')}}</label>
    <select name="city" id="city" required="" class="@error('city') is-invalid @enderror text-field with-border" required>
        <option value="" selected="" disabled="">City</option>
        @if(count($Cities)==0)<option value=""disabled="">Please Choose your country & state first</option>@endif
        @foreach($Cities AS $city)
            <option value="{{ $city }}" >{{ $city }}</option>
        @endforeach
    </select>
    @error('city')<span class="invalid-feedback" role="alert"><strong>{{$message}}</strong></span>@enderror
    <p class="t-1">In case your city is not shown in the field above, please select BEIRUT.</p>

</div>
