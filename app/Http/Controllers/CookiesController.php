<?php

namespace App\Http\Controllers;


use App\Models\FooterCarouselModel;
use Illuminate\Http\Request;

class CookiesController extends Controller
{
    public static function render()
    {

        return view('pages.cookies');
    }
}