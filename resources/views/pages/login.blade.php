@extends('layouts.main')
<title>Tonit Daily | {{ __('title.login') }}</title>
@section('content')

    <section class="account">

        @component('partials.topbanner')@endcomponent
        <div class="content">
           @component('forms.login-account')@endcomponent
               @component('forms.register-account')@endcomponent

        </div>






    </section>
@endsection
