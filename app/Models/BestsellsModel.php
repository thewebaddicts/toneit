<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BestsellsModel extends Model
{
    protected $table='bestsells';
    use HasFactory;
}
