<?php

namespace App\Http\Controllers;
use App\Models\FooterCarouselModel;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public static function checkout()
    {

        if (session('login'))
        return view('pages.checkout.checkout');
        else
            return redirect()->route('login',["notification_id" => 1016]);

    }
    public static function checkoutpayment()
    {

        if(request()->input('lot_id')){

            (new \twa\ecomcart\controllers\EcomCartInformationController())->updateInfo([
                'store' => request()->store['id'],
                'lot_id' => request()->input('lot_id'),
                'shipping_ecom_users_addresses_id' => request()->input('shipping_address'),
                'ecom_stores_delivery_id' => request()->input('shipping_method'),
                'date_delivery' => request()->input('purchase_delivery_note'),
                'delivery_note' => request()->input('purchase_delivery_note'),
            ]);
        }
        return view('pages.checkout.checkout-payment');
    }
    public static function checkoutordersummary()
    {

        if(request()->input('lot_id')) {

            (new \twa\ecomcart\controllers\EcomCartInformationController())->updateInfo([
                 'store' => request()->store['id'],
                 'lot_id' => request()->input('lot_id'),
                'billing_ecom_users_addresses_id' => request()->input('billing_address'),
                'ecom_stores_payment_methods_id' => request()->input('payment_method')
            ]);
        }
        return view('pages.checkout.checkout-order');
    }
}
