<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    public function getCartSummary(){

        $cartList= ecom('cart')->getAsObject();

        return response()->json([
            'status' => true,
            'view' => view('components.checkout.order-summary', ['cartList' => $cartList  , 'type'=> 'checkout'])->render()
        ],200);

    }

    public function getCartDetails(){
        $cartList= ecom('cart')->getAsObject();

        return response()->json([
            'status' => true,
            'view' => view('pages.cart', ['cartList' => $cartList])->render()
        ],200);
    }

    public function updateQuantity(){

        $cartList = ecom('cart')->updateQuantity(request()->cartItemID , request()->direction);



//        $listing = view('pages.cart.listing', ['cartList' => $cartList])->render();
//        $breakdown = view('pages.cart.details', ['cartList'=> $cartList])->render();
        $total = $cartList['count'];

        return response()->json( ['status' => true , 'link' => route('cart') ,'total'=>$total ] , 200);
    }

    public function removeItem(){

        $cartList = ecom('cart')->removeItem(request()->cartItemID);

//        $listing = view('pages.cart.listing', ['cartList' => $cartList])->render();
//        $breakdown = view('pages.cart.details', ['cartList'=> $cartList])->render();
        $total = $cartList['count'];
        return response()->json( ['status' => true , 'link' => route('cart') ,'total'=>$total] , 200);
    }






    public static function GenerateBuildYourBoxLabel($item){
        if(!is_array($item->cms_attributes)){ return false; }
        $WhatsInTheBoxIDs = array_keys($item->cms_attributes);
        $items = DB::table('ecom_products_box_ingredients')->select('id','label','sku')->whereIn('id',$WhatsInTheBoxIDs)->where('cancelled',0)->get();
        $return = [];
        foreach ($items AS $row){
            $return[] = $item->cms_attributes[$row->id]."x ".$row->label." (".$row->sku.")";
        }
        return implode('<br> ',$return);
    }
}
