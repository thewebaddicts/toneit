<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

</head>

<style type="text/css">
    @media print {
        body {
            -webkit-print-color-adjust: exact;
        }
    }
</style>

<body style="font-family: arial,sans-serif; padding:0px; margin:0; font-family:'Raleway',sans-serif">

<div style="background-color:#fff;  width:100%; max-width:750px; padding: 0 40px; margin:auto; overflow:hidden;">
    <table style="border-collapse: collapse; width:100%; margin-bottom:10px">
        <tr>
            <td style="padding:10px 0px ; font-size:26px ; font-weight:bolder ; text-align:left ">
                <img src="{{$data["company_info"]["logo_url"]}}" alt="LOGO" width="170" />
            </td>
            <td
                style="padding:10px 0px ; font-size:15px ;font-family:'Raleway',sans-serif; font-weight:bolder ; text-align:right ">
            </td>
        </tr>
    </table>

    <table style="border-collapse: collapse; width:100%; margin-bottom:20px;  font-size:12px">
        <tr>
            <td style="width : 70%;vertical-align: top;">
                <table style="border-collapse: collapse; width:100%; margin-bottom:20px;  font-size:12px">
                    <tr>
                        <th colspan="2" style="text-align: left;font-size: 14px;  padding: 5px 0; margin: 0 5px">
                            {{$data["company_info"]["company_name"]}}</th>
                    </tr>
                    <tr>
                        <th style="text-align: left;font-size: 12px;">Account # </th>
                        <td>: {{$data["company_info"]["account_number"]}}</td>
                    </tr>
                    <tr>
                        <th style="text-align: left;font-size: 12px;">Email </th>
                        <td>: {{$data["company_info"]["account_email"]}}</td>
                    </tr>
                    <tr>
                        <th style="text-align: left;font-size: 12px;">Address </th>
                        <td>:
                            {{$data["company_info"]["account_address"]}}</td>
                    </tr>
                    <tr>
                        <th style="text-align: left;font-size: 12px;">Order Date</th>
                        <td>: {{$data["order_details"]["date"]}}</td>
                    </tr>
                </table>
            </td>
            <td style="width : 30%;vertical-align: top;">

                <table style="border-collapse: collapse; width:100%; margin-bottom:10px;  font-size:12px">
                    <tr>
                        <th
                            style="text-align: left;font-size: 16px; background-color:#eee; padding: 10px; margin: 0 5px">
                            Shipping Service</th>
                    </tr>
                </table>

                @if($data["shipping_service"])
                    <table style=" width:100%;">
                        <tr style="padding:10px 0">
                            <td style="padding:10px 0"> <img src="{{$data["shipping_service"]["logo_url"]}}"
                                                             alt="Aramex" width="100"> </td>
                        </tr>

                        <tr style="padding:10px 0">
                            <td style="padding:0px 0"> <b> Tracking Number:</b>
                                {{$data["shipping_service"]["tracking_number"]}}</td>
                        </tr>

                        <tr style="padding:10px 0">
                            <td style="padding:10px 0"> <img
                                    src="data:image/png;base64,{{DNS1D::getBarcodePNG($data["shipping_service"]["tracking_number"], 'C128' ) }}"
                                    alt="barcode" />
                                <div style="text-align: center">{{$data["shipping_service"]["tracking_number"]}}</div>
                            </td>
                        </tr>

                    </table>
                @endif
            </td>
        </tr>
    </table>

    <table style="border-collapse: collapse; width:100%; margin-bottom:20px;  font-size:12px">
        <tr>
            <td style="width : 70%;vertical-align: top;">

                <table style="border-collapse: collapse; width:40%; margin-bottom:20px;  font-size:12px">
                    <tr>
                        <th
                            style="text-align: left;font-size: 16px; background-color:#eee; padding: 10px; margin: 0 5px">
                            Shipping details</th>
                    </tr>
                </table>

                <table style="border-collapse: collapse; width:100%; margin-bottom:20px;  font-size:12px">
                    <tr>
                        <th style="text-align: left;font-size: 12px; width:80px">Full Name</th>
                        <td>: {{$data["shipping_details"]["full_name"]}}</td>
                    </tr>
                    <tr>
                        <th style="text-align: left;font-size: 12px" width:80px>Phone </th>
                        <td>: {{$data["shipping_details"]["phone"]}}</td>
                    </tr>
                    <tr>
                        <th style="text-align: left;font-size: 12px" width:80px>City </th>
                        <td>: {{$data["shipping_details"]["city"]}}</td>
                    </tr>
                    <tr>
                        <th style="text-align: left;font-size: 12px" width:80px>Street </th>
                        <td>: {{$data["shipping_details"]["street"]}}</td>
                    </tr>
                    <tr>
                        <th style="text-align: left;font-size: 12px" width:80px>Building </th>
                        <td>: {{$data["shipping_details"]["building"]}}</td>
                    </tr>
                    <tr>
                        <th style="text-align: left;font-size: 12px" width:80px>Delivery Note </th>
                        <td>: {{$data["shipping_details"]["delivery_note"]}}</td>
                    </tr>

                </table>
            </td>
            <td style="width : 30%;vertical-align: top;">
                <table style="border-collapse: collapse; width:100%; margin-bottom:10px;  font-size:12px">
                    <tr>
                        <th
                            style="text-align: left;font-size: 16px; background-color:#eee; padding: 10px; margin: 0 5px">
                            Client Details</th>
                    </tr>
                </table>
                <table style=" width:100%;">
                    <tr>
                        <td> {{ $data["client_details"]["full_name"] }}</td>
                    </tr>
                    <tr>
                        <td> {{ $data["client_details"]["email"] }}</td>
                    </tr>
                    <tr>
                        <td> {{ $data["client_details"]["phone"] }}</td>
                    </tr>

                    <tr>
                        <td>
                            <h2 style="margin: 15px 0 5px 0"> ORDER #: {{ $data["order_details"]["order_number"] }}
                            </h2>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <h2 style="margin: 5px 0 5px 0"> NB ITEMS:
                                {{ $data["order_details"]["quantity_items"] }}</h2>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <h2 style="margin-top: 0px">COD: {{ $data["order_details"]["cod_amount"] }}</h2>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>


    <table style="border-collapse: collapse; width:100%; margin-bottom:0px;  font-size:12px">
        <tr style="">

            <th style="text-align: left;font-size: 12px; background-color: #eee; padding: 10px 5px; margin: 0 5px">
                Image</th>


            <th style="text-align: left;font-size: 12px; background-color: #eee; padding: 10px 0px; margin: 0 5px">
                SKU</th>

            <th style="text-align: left;font-size: 12px; background-color: #eee; padding: 10px 0px; margin: 0 5px">
                Quantity
            </th>
            <th style="text-align: left;font-size: 12px; background-color: #eee; padding: 10px 0px; margin: 0 5px">
                Total
            </th>
        </tr>
        @foreach($data["items"] as $item)
            <tr>
                <td style="border-bottom: 1px solid #eee">
                    <img height='60' width='40' src='{{$item["thumbnail_url"]}}' alt='thumbnail' />
                </td>
                <td style="border-bottom: 1px solid #eee">
                    {{$item["sku"]}}
                </td>
                <td style="border-bottom: 1px solid #eee">
                    {{$item["quantity"]}}
                </td>
                <td style="border-bottom: 1px solid #eee">
                    {{$item["total"]}}
                </td>
            </tr>
        @endforeach
    </table>

    <table style="border-collapse: collapse; width:100%; margin-bottom:30px;  font-size:12px">
        <tr>
            <th
                style="text-align: left;font-size: 16px; height:14px ;background-color:#eee; padding: 10px; margin: 0 5px">
            </th>
        </tr>
    </table>

    <table style="border-collapse: collapse; width:100%; margin-bottom:20px;  font-size:12px">
        <tr>
            <td style="width : 60%;vertical-align: top;">

                <table style="border-collapse: collapse; width:46.6%; margin-bottom:10px;  font-size:12px">
                    <tr>
                        <th
                            style="text-align: left;font-size: 16px; background-color:#eee; padding: 10px; margin: 0 5px">
                            Payment details</th>
                    </tr>
                </table>

                <table style="width : 60%;border-collapse: collapse;  margin-bottom:20px;  font-size:12px">

                    <tr>
                        <td style="padding: 10px 0">
                            <b> Payment method: </b> {{ $data["order_details"]["payment_method"] }}
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width : 40% ;vertical-align: top;">
                <table style="border-collapse: collapse; width:100%; margin-bottom:20px;  font-size:12px">
                    @foreach($data["checkout_details"] as $checkout_item)
                        <tr>
                            <th style="text-align: left; @if($loop->even) background-color: #eee @endif">
                                {{$checkout_item['label']}}</th>
                            <td style="text-align: right; @if($loop->even) background-color: #eee @endif">
                                {{$checkout_item['amount']}}</td>
                        </tr>
                    @endforeach
                </table>
            </td>
        </tr>
    </table>






</div>

</body>

</html>


{{-- <table style="border-collapse: collapse; width:100%; margin-bottom:20px;  font-size:12px">
    <tr>
        <td style="width : 75%;vertical-align: top;">

        </td>
        <td style="width : 25%;vertical-align: top;">

        </td>
    </tr>
</table> --}}
