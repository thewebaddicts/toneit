<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrandsModel extends Model
{
    protected $table='ecom_products_filters';
    use HasFactory;
}
