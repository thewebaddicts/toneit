<?php

namespace App\Http\Controllers;

use App\Models\MainproductModel;
use Illuminate\Http\Request;
use App\Models\SlideshowModel;
use App\Models\ShopbycategoryModel;
use App\Models\BestsellsModel;
use App\Models\FooterCarouselModel;
use App\Models\NavMenuModel;
use App\Models\NewsPageModel;
class HomepageController extends Controller
{
    public static function render()
    {
        $news = NewsPageModel::where('cancelled',0)->limit(2)->get();
        $slideshows = SlideshowModel::where('cancelled', 0)->get();
        $shopbycategory = ShopbycategoryModel::where('cancelled', 0)->limit(4)->get();
        $mainproduct = MainproductModel::where('cancelled', 0)->first();
//        $nav_menu = NavMenuModel::where('cancelled', 0)->get();

        return view('pages.home' , ['mainproduct' => $mainproduct,'slideshows'=>$slideshows,'shopbycategory'=>$shopbycategory,'news'=>$news]);
    }
    
}

