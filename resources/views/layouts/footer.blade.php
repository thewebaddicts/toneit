

<?php


$footercarousel = App\Models\FooterCarouselModel::where('cancelled', 0)->get();
$shopbycategory = App\Models\ShopbycategoryModel::where('cancelled', 0)->limit(4)->get();
?>
@php $socialmedia = getter('social_media')->condition('cancelled',0)->get(); @endphp
<section class="footer">
    <div class="content">
        <h1>Follow Us</h1>
        <h2 class="follow-us"><a class="hover-6" href="{{$socialmedia[0]->instagram_link}}" target="_blank"><span>#tonitdaily</span></a></h2>

{{--        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</p>--}}
        <div class="carousel owl-carousel  footer-carousel" data-carousel-dots="true" data-carousel-items="6"
             data-carousel-loop="true" data-carousel-nav="false" data-carousel-autoplay="true"
             data-carousel-autowidth="true">

            @foreach ($footercarousel as $footercarousels)

                <div class="slide"><a href="{{$footercarousels->link}}"><img
                            src="{{ env('DATA_URL') }}/footer_carousel/{{ $footercarousels->id }}.{{ $footercarousels->extension_image }}?v={{ $footercarousels->version }}' alt=">
                    </a>   </div>
            @endforeach
        </div>

    </div>
</section>


<section class="footermap">
    <div class="content">
        <div class="subscribe"><span>Subscribe to our NewsLetter</span>

            <form class="newsletter-form" action="{{ecom('url')->prefix()}}/newsletter" method="POST">
                @csrf
                <input type="email" name="newsletter-email" placeholder="Your email...">
                <button type="submit">Subscribe</button>
            </form>

            <h4>Follow us</h4>


            <ul class="socialmedia" style="padding-left: 0px;list-style:none;">
                @isset($socialmedia[0]->facebook_link)
                <li><a target="_blank" href={{$socialmedia[0]->facebook_link}}><img src="/assets/svgs/Group 118.svg"></a></li>@endisset
                    @isset($socialmedia[0]->instagram_link)
                <li><a target="_blank" href={{$socialmedia[0]->instagram_link}}><img src="/assets/svgs/instagram.svg"></a></li>@endisset
                    @isset($socialmedia[0]->twitter_link)
                <li><a target="_blank" href={{$socialmedia[0]->twitter_link}}><img src="/assets/svgs/Group 122.svg"></a></li>@endisset
                    @isset($socialmedia[0]->be_link)
                <li><a target="_blank" href={{$socialmedia[0]->be_link}}><img src="/assets/svgs/Group 10.svg"></a></li>@endisset
            </ul>
            <h4>Contact us</h4>
            <ul>
                <li><a href="mailto:support@toneitdaily.com">Email: support@toneitdaily.com</a></li>
                <li><a href="tel:+96181106344">Phone: +96181106344</a></li>

            </ul>
        </div>
        {!!(ecom('menus')->get('footerMenu'))!!}

        {{--        <div class="footercolumnmap">--}}
{{--            <span>Sitemap</span>--}}
{{--            <ul>--}}
{{--                <li><a href={{route('aboutus')}}>About Us</a></li>--}}
{{--                <li><a href={{route('login')}}>Account</a></li>--}}
{{--                <li><a href={{route('news')}}>News</a></li>--}}
{{--                <li><a href={{route('cart')}}> Cart</a></li>--}}
{{--                <li><a href="{{route('account-wishlist')}}" >Wishlist</a></li>--}}
{{--                <li><a href="{{route('contactus')}}">Contact Us</a></li>--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--        <div class="footercolumnmap"><span>Categories</span>--}}
{{--            <ul>--}}
{{--            @foreach ($shopbycategory  as $shopbycategories)--}}
{{--                <li><a href="{{ecom('url')->prefix() }}/products/subcategory/{{ $shopbycategories->id }}/{{ Str::slug($shopbycategories->label) }}">{{$shopbycategories->label}}</a></li>--}}
{{--            @endforeach--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--        <div class="footercolumnmap"><span>Support</span>--}}
{{--           @php $helpandfaq = getter('help_faq')->condition('cancelled',0)->get(); @endphp--}}
{{--            <ul>--}}
{{--                <li><a href="">Shipping and Returns</a></li>--}}

{{--                <li><a href="{{ecom('url')->prefix()}}/helpandfaq/{{$helpandfaq[0]->id}}"> Help and FAQs</a></li>--}}
{{--                <li><a href={{route('termsandconditions')}}>Terms and Conditions</a></li>--}}
{{--                <li><a href={{route('privacypolicy')}}>Privacy Policy</a></li>--}}
{{--                <li><a href={{route('cookies')}}>Cookies</a></li>--}}
{{--            </ul>--}}
{{--        </div>--}}
    </div>
</section>
<sep></sep>
<sep></sep>

<section class="copyright">
    <div class="content">
        <span>©2020. Tonit. All Rights Reserved.</span>
        <span><a style="display: contents" target="_blank" href="https://thewebaddicts.com/">Web Design & Development by The Web Addicts</a></span>
    </div>
</section>