<?php
    if(!isset($value)){ $value = 1; }
    if(!isset($max)){ $max = 9; }
    if(!isset($class)){ $class = ""; }
    if(!isset($inputClass)){ $inputClass = ""; }
    if(!isset($inputName)){ $inputName = "quantity"; }
    if(!isset($allowZero)){ $allowZero = false; }
?>
<div class="ecom_quantity {{ $class }}">
    <input name="{{ $inputName }}" type="number" class="{{ $inputClass }}" value="{{ $value }}" max="{{ $max }}">
    <div class="controls"><a href="javascript:;"
            onclick="@isset($item_id) updateCartQuantity('{{$item_id}}' , -1) @else EcomQuantityDecrease(this,{{ $allowZero }}) @endif">-</a>
        <a href="javascript:;" onclick="@isset($item_id) updateCartQuantity('{{$item_id}}' , 1) @else EcomQuantityIncrease(this)@endif">+</a>
    </div>
</div>

<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

    .ecom_quantity {
        text-align: center;
        position: relative;
        display: inline-block;
        transition:0.3s all;
        border:1px solid #000;
    }

    .ecom_quantity.disabled a:nth-of-type(2){ display: none !important; }

    .ecom_quantity input {
        text-align: center;
        padding: 15px;
        border:0;
        width: calc(100% - 60px);
        position: relative; z-index: 1;
    }

    .ecom_quantity .controls {
        position: absolute;
        display: flex;
        justify-content: space-between;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        z-index: 0;
    }

    .ecom_quantity .controls a {
        display: flex;
        align-content: center;
        align-items: center;
        justify-content: center;
        font-size: 18px;
        color: inherit;
        width: 30px;
        text-align: center;
        text-decoration: none;
        transition: 0.3s all;
    }


    .ecom_quantity .controls a:hover {
        opacity: 0.6;
    }
</style>
