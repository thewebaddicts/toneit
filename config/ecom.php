<?php

return [

    'default_landing' => [
        "mode" => "route",  //this can be either view or route
//        "view" => "pages.select-store",
        "route" => "/lb/en",
    ],


    'menus' => [
        // ['name' => 'footerSocialMenu', 'parameters' => ['menulabel' => 'Footer Menu 2', 'listTag' => 'ul', 'listClass' => 'footer-menu2', 'labelClass' => 'label', 'itemClass' => 'item', 'encloseLI' => true, 'encloseLIClass' => 'encloureClass', 'ecomLinkStructure' => env('APP_URL') . '/{store_prefix}/{language_prefix}/products/list/{id}/{slug}', 'nonEcomLinkPrefix' => '/{store_prefix}/{language_prefix}']],
        ['name' => 'mainMenu', 'parameters' => ['menulabel' => 'Main Menu', 'listTag' => 'ul', 'listClass' => 'main-menu', 'labelClass' => 'label', 'itemClass' => 'item', 'encloseLI' => true, 'encloseLIClass' => 'encloureClass', 'ecomLinkStructure' => env('APP_URL') . '/{store_prefix}/{language_prefix}/products/list/{id}/{slug}', 'nonEcomLinkPrefix' => '/{store_prefix}/{language_prefix}']],
         ['name' => 'footerMenu', 'parameters' => ['menulabel' => 'Footer Menu', 'listTag' => 'ul', 'listClass' => 'footer-menu', 'labelClass' => 'label', 'itemClass' => 'item', 'encloseLI' => true, 'encloseLIClass' => 'encloureClass', 'ecomLinkStructure' => env('APP_URL') . '/{store_prefix}/{language_prefix}/products/list/{id}/{slug}', 'nonEcomLinkPrefix' => '/{store_prefix}/{language_prefix}']],
    ],
    //please fill your route names for the following pages
    'default_routes' =>
        [
            //authentication routes
            'forgot' => "forgot-password",
            'login' => "login",
            'register' => "register",
            //user profile routes
            'addresses' => "account"
        ]
    ,

    'default_views' =>
        [
            'product_details' => "pages.product",
            'product' => "pages.product",
            'address_listing' => "pages.account",
            'address_form' => "pages.account-add-address",
            'address_states' => "addresses.states",
            'address_cities' => "addresses.cities",
            'client_receipts' => 'ecom.receipts.client_default',
             'email_template' => 'ecom.emails.email-template',
            'cart_add_confirmation'=>'components.item-added'
        ],

    //endpoints are the basepath for the apis in case we are using microservices & APIs
    'endpoints' => [
        'storage_url' => env('DATA_URL',env('BASE_DATA_URL',env('APP_URL'))),
        'utilities' => env('API_UTILITIES',env('API_URL')),
        'authentication' => env('API_AUTHENTICATION',env('API_URL')),
        'products' => env('API_PRODUCTS',env('API_URL')),
        'purchases' => env('API_PURCHASE',env('API_URL')),
        'profile' => env('API_PROFILE',env('API_URL')),
    ],

    'caching_tags' => [
        "*/products/query" => "product"
    ]
];
