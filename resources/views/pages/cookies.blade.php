@extends('layouts.main')
@section('content')
    @php

        $cookies = getter('cookies')->get();

    @endphp
    <div class="topbanner">
        <figure>
            <img src="/assets/images/Mask Group 30.png" alt="">
        </figure>
        <figcaption>
            <span>Home / Cookies</span>
            <h1>Cookies</h1>
{{--            <h2> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</h2>--}}

        </figcaption>
    </div>
    <section class="privacycookiesterms">
        <div class="content">
            @foreach($cookies as $cooki)
                <h3>{{$cooki->title}}</h3>
                <p>{{$cooki->text}}
                </p>
            @endforeach

        </div>
    </section>
@endsection
