

@extends('layouts.main')
@section('content')

    <?php $Countries= ecom('addresses')->list();?>

    <div class="topbanner">
        <figure>
            <img src="/assets/images/Mask Group 30.png" alt="">

        </figure>
        <figcaption>
            <span>Home / Checkout</span>
            <h1>Checkout</h1>


        </figcaption>
    </div>
    <div class="content">

        @component('components.checkout-shippingaddress',['Countries'=>$Countries])@endcomponent

    </div>

@endsection