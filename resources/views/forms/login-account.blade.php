<div class="login">

    <h2>Login</h2>
    <form method="post"  action="{{route('login')}}" name="loginform" id="loginform" class="section-form form">
        @csrf
        <label for="email">E-mail</label>
        <input type="hidden" value="{{request()->input('redirect',NULL)}}" name="redirect">
        @if(request()->input('customRouteName'))
            <input type="hidden" name="customRouteName" id="" value="{{request()->input('customRouteName')}}">
        @endif
        <input type="email" name="email" placeholder="Your E-mail" id="email">
        <label for="password">Password</label>
        <input type="password" placeholder="Your Password" name="password" id="password">
        <a href="{{route('forgot-password')}}" style="text-align: right">Forgot Password?</a>
        <input type="submit" value="Log in">

    </form>
    <span>or</span>
    <section>
        <a href="{{route('login.facebook')}}">
        <div class="facebook-google-login">
            <picture>
               <img src="/assets/svgs/Group 118.svg" alt="">
            </picture>
            <div><span>Log in with Facebook</span></div>
        </div>
        </a>
        <a href="{{route('login.google')}}">
        <div class="facebook-google-login">
            <picture>
                <img src="/assets/svgs/Group 120.svg" alt="">
            </picture>
            <div><span>Log in with Google</span></div>
        </div>
        </a>
    </section>
</div>