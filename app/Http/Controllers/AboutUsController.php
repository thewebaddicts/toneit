<?php

namespace App\Http\Controllers;

use App\Models\AboutUsModel;

use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public static function render()
    {
        $aboutus= AboutUsModel::where('cancelled', 0)->get();

        return view('pages.aboutus',['aboutus'=>$aboutus]);
    }
}

