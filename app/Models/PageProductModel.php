<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageProductModel extends Model
{
    protected $table = 'products_page';
    use HasFactory;
    public function getImageAttribute()
    {
        return  env('DATA_URL') . "/products_page/" . $this->id . "." . $this->extension_image . "?v=." . $this->version;
    }
    // public function getImageResizedAttribute()
    // {
    //     return  env('DATA_URL') . "/blogs_folder_400_280/" . $this->id . ".jpg?v=." . $this->version;
    // }
}
