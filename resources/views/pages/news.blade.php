@extends('layouts.main')

@section('content')
    <div class="topbanner">
        <figure>
            <img src="/assets/images/Mask Group 30.png" alt="">
        </figure>
        <figcaption>
            <span>Home / News</span>
            <h1>News</h1>
            <h2> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</h2>

        </figcaption>
    </div>

    <section class="news">
        <div class="content">

            @component('components.sortby')@endcomponent

            <main>

                @foreach($news as $new)
                    @component('partials.product-article',[ "new" => $new ]) @endcomponent
                @endforeach

            </main>
            <div class="d-flex justify-content-center">
                {!!$news->links()!!}
            </div>
        </div>
    </section>

@endsection
