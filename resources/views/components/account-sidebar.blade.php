<ul>
    <li><a href={{route('account')}}>Account Details</a>
        @if(Request::url() === route('account') || Request::url() === route('account-add-address'))
            <img id="arrow-right" src="/assets/svgs/Icon ionic-ios-arrow-back.svg">
        @endif
    </li>

    <li><a href={{route('account-address')}}>Addresses</a>
        @if(Request::url() === route('account-address'))
            <img id="arrow-right" src="/assets/svgs/Icon ionic-ios-arrow-back.svg">
        @endif
    </li>
    <li><a href={{route('account-wishlist')}}>Wishlist</a>
        @if(Request::url() ===route('account-wishlist'))
            <img id="arrow-right" src="/assets/svgs/Icon ionic-ios-arrow-back.svg">
        @endif</li>
    <li><a href={{route('account-order')}}>Orders</a>
        @if(Request::url() ===route('account-order'))
            <img id="arrow-right" src="/assets/svgs/Icon ionic-ios-arrow-back.svg">
        @endif</li>
    <li><a href={{route('logout')}}>Logout</a>
        @if(Request::url() ===route('logout'))
            <img id="arrow-right" src="/assets/svgs/Icon ionic-ios-arrow-back.svg">
        @endif</li>
</ul>
<form style="display: contents">
    <select id="sort-by-name" onchange="window.location.href=this.value;">

        <option    @if(Request::url() === route('account') || Request::url() === route('account-add-address')) selected @endif value="{{route('account')}}">Account Details</option>
        <option    @if(Request::url() === route('account-address'))selected  @endif value="{{route('account-address')}}">Addresses</option>
        <option  @if(Request::url() ===route('account-wishlist'))  selected  @endif value="{{route('account-wishlist') }}">Wishlist</option>
        <option @if(Request::url() ===route('account-order')) selected  @endif value="{{route('account-order')}}">Orders</option>
        <option  value="{{route('logout')}}">Logout</option>
    </select>
</form>