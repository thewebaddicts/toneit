<div class="checkout">


    <?php $PaymentMethods = ecom('stores')->getPayments(); ?>
    <?php $ShippingMethods = ecom('stores')->getShipping();

 $cartList = ecom('cart')->getAsArray();

//dd($cartList);

    ?>

    <picture>
        <div>
            <div class="parent">
                <div style="visibility: hidden" class="prog-bar"></div>
                <div style="background: #998484;color: white" class="square">1</div>
                <div style="background: #998484;" class="prog-bar"></div>
            </div>

            <span>Shipping Address</span>

        </div>

        <div>
            <div class="parent">
                <div style="background: #998484;" class="prog-bar"></div>
                <div style="background: #998484;color: white" class="square">2
                </div>
                <div style="background: #998484;" class="prog-bar"></div>
            </div>

            <span>Payment Method</span>
            <div class="progress-bar"></div>
        </div>

        <div>
            <div class="parent">
                <div class="prog-bar"></div>
                <div style="" class="square">3</div>
                <div style="visibility: hidden" class="prog-bar"></div>
            </div>

            <span>Order Summary</span>
            <div class="progress-bar"></div>
        </div>
    </picture>

    <form style="width:100%;" method="get" action={{route('checkout-order')}} >
        <input type="hidden" name="lot_id" value="{{$cartList['info']['lot_id']}}">
        <input type="hidden" name="shipping_address" value="{{request()->shipping_address}}">

        <div class="paymentmethod">
            @foreach($PaymentMethods as $pay)


                <div>
                    <label class="container">
                        <input id="radio" type="radio" name="payment_method" value="{{$pay->id}}">
                        <span class="checkmark"></span>
                    </label>


                    <div>{{$pay->label}}</div>

                </div>
                <sep></sep>
                <sep></sep>
                <sep></sep>
                <sep></sep>
                <sep></sep>
            @endforeach



        </div>
        <sep></sep>
        <span class="small-message">Please choose your payment method</span>
        <h3>Billing Address</h3>
        <main>
            <?php if (isset($_GET['address'])) {
                $address = $_GET['address'];
            }
            ?>



            @foreach($Countries as $Country)
                <div id="remove-address-second" class="sub-box">
                    <header>
                        <span>

                            <label class="container">
                <input type="radio" checked="checked" name="billing_address" value="{{$Country->id}}">
                <span class="checkmark"></span>
            </label>


                            <div></div></span>

                        {{--                        <div><span class="edit-feature" data-target="edit-address-second"><img--}}
                        {{--                                        src="/assets/svgs/Icon feather-edit.svg" alt=""></span><span--}}
                        {{--                                    class="trash" data-target="remove-address-second"><img--}}
                        {{--                                        src="/assets/svgs/Icon feather-trash-2.svg" alt=""></span></div>--}}
                    </header>

                    <?php $loop->index;?>

                    <p>Address {{$loop->index+1}}</p>
                    <p>{{$Country->country->name}} | {{$Country->city}} | {{$Country->street}} | {{$Country->building}}
                        Building | {{$Country->floor}} Floor</p>
                </div>
            @endforeach
        </main>
        <sep></sep>
        <sep></sep>
        <sep></sep>
        <div class="btns">
            <a href={{route('checkout')}}>Previous</a>
            <a href="{{route('account-addresses',['redirect'=>route('checkout')])}}">Add Address</a>
            <input type="hidden" name="shipping_method" value="2">
            <input id="submit" type="submit" value="Next">
        </div>
    </form>

<script>
    function FooterFunctions(){
        disableButton2();


    }



</script>